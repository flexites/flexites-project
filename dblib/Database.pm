package _projectName_::dblib::Database;

use utf8;
use strict;

use base qw(Engine::Database);

use SQL4Services::Session;
use SQL4Services::SysoptionNew;
use SQL4Services::UserSettings;
use SQL4Services::Page;
use SQL4Services::Secure;
use SQL4Services::Event;
use SQL4Services::Tools;
use SQL4Services::XCatNew;
use SQL4Services::XCatNewArchive;
use SQL4Services::FilesNew;
use SQL4Services::Cookies;

#--------------------------------------------------------------------------

sub new {
    my $proto = shift;
    my $class = ref($proto) || $proto;

    my $self = $class->SUPER::new(@_);
    bless $self, $class;

    return $self;

}

1;
