package _projectName_::handlers::Forms;

use utf8;
use strict;

use base qw(Handlers::Forms);

use Engine::GlobalVars;


sub new {
    my $proto = shift;
    my $class = ref($proto) || $proto;
    my $self = $class->SUPER::new(@_);
    bless $self, $class;
    return $self;
}

sub ProcessForm {
    my $self = shift;
    return $self->StandartForm(@_);
}

sub CaptchaTest {
    my $self = shift;
    my ($value, $form_name) = @_;

    my $ret = ($App->Data->Var('__AJAX_REQUEST') or ($value and $App->Session->Var('__CAPTCHA_SECRET'.$form_name) and $App->Session->Var('__CAPTCHA_SECRET'.$form_name) eq $value));
    $App->Session->UndefVar('__CAPTCHA_SECRET'.$form_name) unless ($ret or $App->Data->Var('__AJAX_REQUEST'));

    return $ret;
}

sub AgreementTest {
    my $self = shift;
    my ($value, $form_name) = @_;
    return !!$value;
}

1;
