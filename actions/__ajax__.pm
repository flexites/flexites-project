package _projectName_::actions::__ajax__;

use utf8;
use strict;

use base qw(Engine::Action);

use Engine::GlobalVars;
use Engine::SysUtils;
use Engine::Xslate;
use Engine::Xslate::Functions qw(XslateFunctions);

use _projectName_::actions::index;


sub new {
    my $proto = shift;
    my $class = ref($proto) || $proto;

    my $self = $class->SUPER::new(@_);
    bless $self, $class;

    return $self;
}

#
# -----------------------------------------------------------------------
#

sub Init {
    my $self = shift;
    $self->DefineDefaults();
    $self->DefineVars();
}

sub Run {
    die "Must define your own method";
}

#
# -----------------------------------------------------------------------
#

sub DefineDefaults {
    my $self = shift;
    $App->Data->Var('__AJAX_REQUEST', 1);
    $self->Index->DefineDefaults();
}
sub DefineVars {
    my $self = shift;
    $self->Index->DefineVars();
}

#
# -----------------------------------------------------------------------
#

sub CustomController {
    my $self = shift;
    my ($controller_name, $template_name) = @_;
    return $self->Index->CustomController($controller_name, $template_name);
}

#
# -----------------------------------------------------------------------
#

sub Template {
    my $self = shift;
    if (!defined $self->{'__TEMPLATE'}) {
        $self->{'__TEMPLATE'} = Engine::Xslate->new(function => XslateFunctions(), @_);
    }
    return $self->{'__TEMPLATE'};
}

sub Index {
    my $self = shift;
    if (!exists $self->{'__INDEX'}) {
        $self->{'__INDEX'} = _projectName_::actions::index->new(@_);
    }
    return $self->{'__INDEX'};
}

sub GetNodeByHref {
    my $self = shift;
    my ($href) = @_;

    my $url = (split /\?/, $href)[0];
    my @url = grep { $_ ne '' } (split /\//, $url);
    my $node_id = GetNodeByURLPath($App->Param('i_c'), \@url);

    return $node_id;
}

1;
