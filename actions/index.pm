package _projectName_::actions::index;

use utf8;
use strict;

use Engine::GlobalVars;
use base qw(Actions::index);


sub new {
    my $proto = shift;
    my $class = ref($proto) || $proto;
    my $self = $class->SUPER::new(@_);
    return $self;
}

sub GetCurrents {
    my $self = shift;
}

sub CustomController {
    my $self = shift;
    my ($controller_name, $template_name) = @_;

    print "[ _projectName_::actions::index::CustomController ] $controller_name, $template_name\n";

    my $controller_file = $project.'::'.$Conf->Param('common/controllers').'::'.$controller_name;
    eval "require $controller_file";
    die $@ if ($@);
    return $controller_file->new($template_name);
}

1;
