package _projectName_::actions::json;

use utf8;
use strict;

use base qw(_projectName_::actions::__ajax__);

use Engine::GlobalVars;
use Engine::Helpers;

use JSON;


sub new {
    my $proto = shift;
    my $class = ref($proto) || $proto;

    my $self = $class->SUPER::new(@_);
    bless $self, $class;

    return $self;
}

#
# -----------------------------------------------------------------------
#

sub Init {
    my $self = shift;
    $self->SUPER::Init(@_);

    my $action = $App->NodePath(1);
    my $act = 'H_'.$action;
    my $res = {};
    eval { $res = $self->$act(); };
    if ($@) {
        print "[ ERROR ] [ _projectName_::actions::json::Init ] Error in $act: $@\n";
        $res = {};
        $self->Status(404);
    }
    $self->{'json'} = $res;
}

sub Run {
    my $self = shift;
    $App->ContentType('application/json');
    $self->Text(encode_json($self->{'json'} || {}));
}

#
# -----------------------------------------------------------------------
#

sub H_get_form {
    my $self = shift;

    if (!defined $App->Param('form_id') and defined $App->Param('href')) {
        $App->Param('form_id', $self->GetNodeByHref($App->Param('href')));
    }

    if (my $obj = $DB->CAT_GET_OBJECT(object_id => $App->Param('form_id'))->fetchrow_hashref()) {
        my $controller = $self->CustomController($obj->{'class_name'}, $obj->{'class_name'});
        my $obj = $controller->GetObject($obj->{'object_id'});

        my $data = {
            object => $obj
        };

        my ($content, $error) = $self->Template->Content(template => 'ajax/'.$obj->{'class_name'}.'.tx', data => $data);

        return { errors => { template => 1 } } if ($error);
        return { ok => 1, content => $content };
    }

    return { errors => { form_id => 1 } };
}

sub H_send_form {
    my $self = shift;

    if (my $obj = $DB->CAT_GET_OBJECT(object_id => $App->Param('form_id'))->fetchrow_hashref()) {
        my $controller = $self->CustomController($obj->{'class_name'}, $obj->{'class_name'});
        my $obj = $controller->GetObject($obj->{'object_id'}, { skip_construction => 1 });

        if ($obj->{'_form_sent'}) {
            return {
                ok => 1,
                content => $obj->{'_s_content'}
            };
        }

        return {
            errors => $obj->{'errors'}
        };
    }

    return { errors => { form_id => 1 } };
}

1;
