/**

# Gulpfile.js
Конфигурационный файл для сборки front-end


## Tasks
Список задач

### Public tasks:
- watch-js - запуск watcher JS для development
- prod-js - сборка JS для production
- dev-js - сборка JS для development
- lint-js - линтинг JS

- watch-css - запуск watcher CSS для development
- prod-css - сборка CSS для production
- dev-css - сборка CSS для development
- lint-css - линтинг CSS

- watch - запуск watcher для development
- prod - сборка для production
- dev - сборка для development
- lint - линтинг

**/

const gulp = require('gulp'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    sourcemaps = require('gulp-sourcemaps'),
    plumber = require('gulp-plumber'),
    path = require('path'),
    assign = require('lodash.assign');

const browserify = require('browserify'),
    babelify = require('babelify'),
    watchify = require('watchify'),
    source = require('vinyl-source-stream'),
    buffer = require('vinyl-buffer'),
    uglify = require('gulp-uglify'),
    eslint = require('gulp-eslint');

const postcss = require('gulp-postcss'),
    cleanCSS = require('gulp-clean-css'),
    extractMedia = require('postcss-extract-media-query'),
    stylelint = require('stylelint');

const js = {
    dir: './js',
    srcDir: 'src',
    bundleDir: 'bundle',
    projects: {
        main: {
            srcFile: 'main.js',
            bundleFile: 'bundle.js',
            resultFile: 'main.js',
            files: [
                'lib/jquery3.js',
                'lib/jquery3/plugins/jquery-migrate-3.0.0.min.js',
                'lib/jquery/plugins/fancybox2/jquery.fancybox.pack.js',

                'lib/polyfills/flexibility.js',
                'lib/polyfills/object-fit-images.js'
            ]
        }
    },
    modes: {
        development: {
            resultDir: 'public',
            mapDir: 'maps',
            mapOptions: {},
            uglifyOptions: {
                output: {
                    beautify: true
                }
            }
        },
        production: {
            resultDir: 'dist',
            mapDir: 'maps',
            mapOptions: {},
            uglifyOptions: {
                compress: {
                    pure_funcs: ['console.log']
                }
            }
        }
    },
    lintOptions: {

    },
    babelifyOptions: {
        presets: [['env']]
    }
};

const css = {
    dir: './css',
    srcDir: 'src',
    projects: {
        main: {
            srcFile: 'main.pcss',
            resultFile: 'main.css'
        }
    },
    modes: {
        development: {
            resultDir: 'public',
            mapDir: 'maps',
            mapOptions: {},
            mediaOptions: {
                combine: true,
                whitelist: true,
                queries: {
                    '(max-width:480px)': '480',
                    '(max-width:1024px)': '1024'
                }
            },
            cleanOptions: {
                restructuring: false,
                aggressiveMerging: false,
                compatibility: 'ie10+',
                format: 'beautify'
            }
        },
        production: {
            resultDir: 'dist',
            mapDir: 'maps',
            mapOptions: {},
            mediaOptions: {
                combine: true,
                whitelist: true,
                queries: {
                    '(max-width:480px)': '480',
                    '(max-width:1024px)': '1024'
                }
            },
            cleanOptions: {
                restructuring: true,
                aggressiveMerging: true,
                compatibility: 'ie10+'
            }
        }
    },
    plugins: [
        ['postcss-import'],
        ['postcss-mixins'],
        ['postcss-nested'],
        ['postcss-simple-vars'],
        ['postcss-hexrgba'],
        ['postcss-math', { functionName: 'calculate' }],
        ['postcss-conditionals'],
        ['postcss-preset-env', { stage: 4, autoprefixer: { grid: true, flexbox: true } }],
        ['postcss-object-fit-images'],
        ['postcss-flexibility']
    ],
    lintOptions: {

    }
};

const processors = [];
for (let plugin of css.plugins) {
    let module = plugin.shift(),
        processor = require(module),
        args = plugin;
    processors.push(processor.apply(global, args));
}

const pass = function() {};
const check = function(count, length, callback) {
    return function() {
        count ++;
        if (count === length) {
            callback();
        }
    };
};


function buildES6(mode, project, watch, callback=pass) {
    let opts = {
        entries: [ path.join(js.dir, js.srcDir, js.projects[project].srcFile) ],
        paths: [ path.join(js.dir, js.srcDir) ],
        transform: [ babelify.configure(js.babelifyOptions) ],
        debug: true
    };
    if (watch) {
        opts = assign({}, watchify.args, opts);
    }
    let bundler = watch ? watchify(browserify(opts)) : browserify(opts);

    function rebundle() {
        console.log('buildES6', mode, project);

        let stream = bundler.bundle();
        return stream.on('error', console.error)
            .pipe(source(js.projects[project].bundleFile))
            .pipe(buffer())
            .pipe(gulp.dest(path.join(js.dir, js.bundleDir)))
            .on('end', callback);
    }
    bundler.on('update', function() {
        rebundle();
    });
    bundler.on('log', console.log);
    rebundle();
}
function concatJS(mode, project, callback=pass) {
    console.log('concatJS', mode, project);

    const files = js.projects[project].files.concat(path.join(js.bundleDir, js.projects[project].bundleFile));

    const mapOptions = assign({}, js.modes[mode].mapOptions, {
        sourceMappingURLPrefix: '/' + path.join(js.dir, js.modes[mode].resultDir)
    });

    gulp.src(files, { cwd: js.dir })
        .pipe(sourcemaps.init({ loadMaps: true }))
        .pipe(concat(js.projects[project].resultFile))
        .pipe(uglify(js.modes[mode].uglifyOptions))
        .pipe(sourcemaps.write(js.modes[mode].mapDir, mapOptions))
        .pipe(gulp.dest(path.join(js.dir, js.modes[mode].resultDir)))
        .on('end', callback);
}
function buildJS(mode, project, callback=pass) {
    buildES6(mode, project, false, () => {
        concatJS(mode, project, callback);
    });
}
function lintJS(callback=pass) {
    console.log('lintJS');

    let files = [
        path.join(js.dir, js.srcDir, '**', '*.js')
    ];

    gulp.src(files)
        .pipe(eslint(js.lintOptions))
        .pipe(eslint.format())
        .on('end', callback);
}

function buildPostCSS(mode, project, callback=pass) {
    console.log('buildPostCSS', mode, project);

    const mapOptions = assign({}, css.modes[mode].mapOptions, {
        sourceMappingURLPrefix: '/' + path.join(css.dir, css.modes[mode].resultDir)
    });

    gulp.src(path.join(css.dir, css.srcDir, css.projects[project].srcFile))
        .pipe(sourcemaps.init())
        .pipe(plumber({ errorHandler: console.error }))
        .pipe(postcss(processors))
        .pipe(cleanCSS(css.modes[mode].cleanOptions))
        .pipe(plumber.stop())
        .pipe(rename(css.projects[project].resultFile))
        .pipe(sourcemaps.write(css.modes[mode].mapDir, mapOptions))
        .pipe(gulp.dest(path.join(css.dir, css.modes[mode].resultDir)))
        .on('end', callback);
}
function extractCSSMedia(mode, project, callback=pass) {
    console.log('extractCSSMedia', mode, project);

    let parts = css.projects[project].resultFile.split(/(\.)/);
    parts.splice(parts.length - 2, 0, '--media');

    const mediaOptions = assign({}, css.modes[mode].mediaOptions, {
        output: {
            path: path.join(__dirname, css.dir, css.modes[mode].resultDir)
        }
    });

    gulp.src(path.join(css.dir, css.modes[mode].resultDir, css.projects[project].resultFile))
        .pipe(rename(parts.join('')))
        .pipe(postcss([ extractMedia(mediaOptions) ]))
        .pipe(gulp.dest(path.join(css.dir, css.modes[mode].resultDir)))
        .on('end', callback);
}
function buildCSS(mode, project, callback=pass) {
    buildPostCSS(mode, project, () => {
        extractCSSMedia(mode, project, callback);
    });
}
function lintCSS(callback=pass) {
    console.log('lintCSS');

    let files = [
        path.join(css.dir, css.srcDir, '**', '*.pcss')
    ];

    gulp.src(files)
        .pipe(postcss([stylelint(css.lintOptions)]))
        .on('end', callback);
}


gulp.task('watch-js', function(done) {
    for (let project in js.projects) {
        let files = [
            path.join(js.dir, js.bundleDir, js.projects[project].bundleFile)
        ];
        for (let file of js.projects[project].files) {
            files.push(path.join(js.dir, file));
        }
        const watcher = gulp.watch(files, { awaitWriteFinish: true });
        watcher.on('change', function(path) {
            concatJS('development', project);
        });
        buildES6('development', project, true);
    }
});
gulp.task('prod-js', function(done) {
    let c = 0,
        callback = check(c, Object.keys(js.projects).length, done);

    for (let project in js.projects) {
        buildJS('production', project, callback);
    }
});
gulp.task('dev-js', function(done) {
    let c = 0,
        callback = check(c, Object.keys(js.projects).length, done);

    for (let project in js.projects) {
        buildJS('development', project, callback);
    }
});
gulp.task('lint-js', function(done) {
    lintJS(done);
});


gulp.task('watch-css', function(done) {
    function rebuild() {
        for (let project in css.projects) {
            buildCSS('development', project);
        }
    }
    let files = [
        path.join(css.dir, css.srcDir, '**', '*.pcss'),
        path.join(css.dir, css.srcDir, '**', '*.css')
    ];
    const watcher = gulp.watch(files, { awaitWriteFinish: true });
    watcher.on('change', function(path) {
        rebuild();
    });
    rebuild();
});
gulp.task('prod-css', function(done) {
    let c = 0,
        callback = check(c, Object.keys(css.projects).length, done);

    for (let project in css.projects) {
        buildCSS('production', project, callback);
    }
});
gulp.task('dev-css', function(done) {
    let c = 0,
        callback = check(c, Object.keys(css.projects).length, done);

    for (let project in css.projects) {
        buildCSS('development', project, callback);
    }
});
gulp.task('lint-css', function(done) {
    lintCSS(done);
});


gulp.task('watch', gulp.parallel('watch-js', 'watch-css'));
gulp.task('prod', gulp.parallel('prod-js', 'prod-css'));
gulp.task('dev', gulp.parallel('dev-js', 'dev-css'));
gulp.task('lint', gulp.series('lint-js', 'lint-css'));
