'use strict';

/* global $ */


import { interfaceName } from './settings';

import { AjaxForm } from './modules/ajax-form';
import { Goal } from './modules/goal';
import { Responsive } from './modules/responsive';


class Proj {
    constructor() {
        this.init();
    }
    init() {
        this.interfaceName = interfaceName;

        this.ajaxForm = new AjaxForm();
        this.goal = new Goal({
            goals: [
                {
                    selector: '.js-goal__link',
                    event: 'click',
                    handler() {
                        $(window).trigger('goal.main', $(this).data());
                    }
                }, {
                    selector: '.js-goal__form',
                    event: 'submit',
                    handler() {
                        $(window).trigger('goal.main', $(this).data());
                    }
                }
            ]
        });

        this.responsive = new Responsive();
    }
}

$(function() {
    window.proj = new Proj();
});
