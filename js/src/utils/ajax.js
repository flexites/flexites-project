'use strict';

/* global $ */

import { ajax } from 'settings';


class Ajax {
    static get(act, data, options) {
        console.log('[ Ajax ] get', act, data);

        let opt = $.extend({}, ajax, options);

        if (opt.SESSION_ID) {
            if (data instanceof Array) {
                data.push({ name: 'SESS_ID', value: opt.SESSION_ID });
            } else if (data instanceof FormData) {
                data.append('SESS_ID', opt.SESSION_ID);
            } else if (data instanceof URLSearchParams) {
                data.append('SESS_ID', opt.SESSION_ID);
            } else if (typeof data === 'string') {
                if (data != '') data += '&';
                data += 'SESS_ID='+opt.SESSION_ID;
            } else if (typeof data === 'object') {
                data.SESS_ID = opt.SESSION_ID;
            } else {
                throw new Error('Can\'t append SESSION_ID to this type of data');
            }
        }
        delete opt.SESSION_ID;

        if (!/^\//.test(act)) {
            opt.url += act;
        } else {
            opt.url = act;
        }
        if (!/\/$/.test(opt.url)) {
            opt.url += '/';
        }

        if (data instanceof FormData || data instanceof URLSearchParams) {
            opt.contentType = false;
            opt.processData = false;
        }

        opt.data = data;

        let handler = new Promise(function(resolve, reject) {
            opt.success = function(data, textStatus, jqXHR) {
                resolve(data, jqXHR);
            };
            opt.error = function(jqXHR) {
                reject(jqXHR.statusText, jqXHR);
            };
            $.ajax(opt);
        });

        return handler;
    }
}


export { Ajax };
