'use strict';

/* global $ */


let browserType = $(document.body).data('browser');


class Browser {
    static get type() {
        return browserType;
    }
    static isDesktop() {
        return this.type === 'desktop';
    }
    static isTablet() {
        return this.type === 'tablet';
    }
    static isMobile() {
        return this.type === 'mobile';
    }
    static isDevice() {
        return this.isMobile() || this.isTablet();
    }
}


export {
    Browser
};
