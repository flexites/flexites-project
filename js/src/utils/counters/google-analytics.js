'use strict';

/* global $ */

import { AbstractCounter } from './abstract-counter';


class GoogleAnalytics extends AbstractCounter {
    constructor(options) {
        super($.extend({}, {counterName: 'ga'}, options));
    }
    reachGoal(goal, params={}) {
        console.log('[ GoogleAnalytics ] reachGoal', goal, params);

        try {
            this.counter('send', 'event', goal, params);
            console.info(`[ GoogleAnalytics ] Goal ${goal} is reached successfully`);
        } catch (err) {
            console.error(`[ GoogleAnalytics ] Can't reach goal ${goal}`, err);
        }
    }
    setParams(params) {
        console.log('[ GoogleAnalytics ] setParams', params);

        try {
            this.counter('set', params);
            console.info('[ GoogleAnalytics ] Params are set successfully');
        } catch (err) {
            console.error('[ GoogleAnalytics ] Can\'t set params', err);
        }
    }
    get type() {
        return 'google-analytics';
    }
}


export {
    GoogleAnalytics
};
