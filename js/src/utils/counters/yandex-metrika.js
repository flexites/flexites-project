'use strict';

/* global $ */

import { yaCounter } from 'settings';

import { AbstractCounter } from './abstract-counter';


class YandexMetrika extends AbstractCounter {
    constructor(options) {
        super($.extend({}, { counterName: yaCounter}, options));
    }
    reachGoal(goal, params={}) {
        console.log('[ YandexMetrika ] reachGoal', goal, params);

        try {
            this.counter.reachGoal(goal, params);
            console.info(`[ YandexMetrika ] Goal ${goal} is reached successfully`);
        } catch (err) {
            console.error(`[ YandexMetrika ] Can't reach goal ${goal}`, err);
        }
    }
    setParams(params) {
        console.log('[ YandexMetrika ] setParams', params);

        try {
            this.counter.params(params);
            console.info('[ YandexMetrika ] Params are set successfully');
        } catch (err) {
            console.error('[ YandexMetrika ] Can\'t set params', err);
        }
    }
    get type() {
        return 'yandex-metrika';
    }
}


export {
    YandexMetrika
};
