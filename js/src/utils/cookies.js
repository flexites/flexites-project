'use strict';

const EXPIRES_REGEXP = /\+(\d+)(s|i|h|d|m|y)/;


class Cookies {
    /**
     * Устанавливает значение cookie
     * @param {String} name
     * @param {String} value
     * @param {Object} options
     */
    static set(name, value, options={}) {
        console.log('[ Cookies ] set', name, value);

        if (value === null || value === undefined) {
            this.delete(name);
        } else {
            if (options.expires) {
                options.expires = this.__getExpires(options.expires);
            }
            let cookie = name + '=' + escape(value) +
                (options.expires ? '; expires=' + options.expires : '') +
                (options.path ? '; path=' + options.path : '') +
                (options.domain ? '; domain=' + options.domain : '') +
                (options.secure ? '; secure' : '');
            document.cookie = cookie;
        }
    }
    /**
     * Получает значение cookie
     * @param {String} name
     */
    static get(name) {
        console.log('[ Cookies ] get', name);

        let cookie = ' ' + document.cookie;
        let search = ' ' + name + '=';
        let setStr = null;
        let offset = 0;
        let end = 0;
        if (cookie.length > 0) {
            offset = cookie.indexOf(search);
            if (offset !== -1) {
                offset += search.length;
                end = cookie.indexOf(';', offset);
                if (end === -1)
                    end = cookie.length;
                setStr = unescape(cookie.substring(offset, end));
            }
        }
        return setStr;
    }
    /**
     * Удаляет значение cookie
     * @param {String} name
     */
    static delete(name) {
        console.log('[ Cookies ] delete', name);

        this.set(name, '', { expires: -1 });
    }
    /**
     * Получает дату истечения cookie
     * @param {String|Number|Date} expires
     */
    static __getExpires(expires) {
        if (typeof expires === 'string') {
            let exp = expires.match(EXPIRES_REGEXP);
            if (exp) {
                let d = new Date();
                let c = parseInt(exp[1]);
                let s = exp[2];

                switch (s) {
                case 'y':
                    d.setFullYear(d.getFullYear() + c);
                    break;
                case 'm':
                    d.setMonth(d.getMonth() + c);
                    break;
                case 'd':
                    d.setDate(d.getDate() + c);
                    break;
                case 'h':
                    d.setHours(d.getHours() + c);
                    break;
                case 'i':
                    d.setMinutes(d.getMinutes() + c);
                    break;
                case 's':
                    d.setSeconds(d.getSeconds() + c);
                    break;
                }
                expires = d.toUTCString();
            }
        } else if (typeof expires === 'number') {
            let d = new Date();
            d.setTime(d.getTime() + expires);
            expires = d.toUTCString();
        } else if (expires instanceof Date) {
            expires = expires.toUTCString();
        } else {
            throw new Error('Can\'t use this type of expires option');
        }
        return expires;
    }
}


export {
    Cookies
};
