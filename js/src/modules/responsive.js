'use strict';

/* global $ */

import { breakpoints } from 'settings';


class Responsive {
    constructor(options) {
        this.opt = $.extend({}, { breakpoints }, options);
        this.currentPoint = null;
        this.init();
    }
    init() {
        console.log('[ Responsive ] init');

        this.$window.on('resize', () => {
            this._watchSize();
        });
        this._watchSize();
    }
    _watchSize() {
        let width = this.getWidth(),
            point = this.getPoint(width);

        if (!this.currentPoint || point !== this.currentPoint) {
            if (this.currentPoint) {
                console.log(`[ Responsive ] Off: ${this.currentPoint}`);
                this.$window.trigger('off:' + this.currentPoint + '.responsive');
            }

            console.log(`[ Responsive ] On: ${point}`);
            this.$window.trigger('on:' + point + '.responsive');

            if (this.currentPoint) {
                this.$window.trigger('change.responsive', { point: point });
            } else {
                this.$window.trigger('init.responsive', { point: point });
            }

            this.currentPoint = point;
        }
    }
    getWidth() {
        return this.$window.width();
    }
    getPoint(width) {
        let point = this.points[0];
        for (let i = this.points.length - 1; i > 0; i --) {
            if (width > this.opt.breakpoints[this.points[i - 1]] && width <= this.opt.breakpoints[this.points[i]]) {
                point = this.points[i];
                break;
            }
        }
        return point;
    }
    get points() {
        if (!this.__points) {
            this.__points = [];
            for (let k in this.opt.breakpoints) {
                this.__points.push(k);
            }
            this.__points.sort((a, b) => {
                return (this.opt.breakpoints[a] < this.opt.breakpoints[b]) ? -1 : ((this.opt.breakpoints[a] > this.opt.breakpoints[b]) ? 1 : 0);
            });
        }
        return this.__points;
    }
    get $window() {
        if (!this.__$window) {
            this.__$window = $(window);
        }
        return this.__$window;
    }
}


export {
    Responsive
};
