'use strict';

/* global $ */

import { Ajax } from 'utils/ajax';


let settings = {
    fieldSelector: '.js-ajax-form__field',
    fieldNamePrefix: '.js-ajax-form__field--',
    contentSelector: '.js-ajax-form__content',
    errorClass: 'is-error'
};


class Form {
    constructor(el, options) {
        this.el = el;
        this.opt = $.extend({}, settings, options);
        this.init();
    }
    init() {
        console.log('[ Form ] init', this.el);

        this.$form = $(this.el);
        this.$fields = this.opt.$fields || $(this.opt.fieldSelector, this.el);
        this.$content = this.opt.$content || $(this.opt.contentSelector).filter('[data-form_id="' + this.$form.data('form_id') + '"]');

        this._initFields();
        this._initEvents();

        if ($.fancybox.isOpen) {
            $.fancybox.update();
        }
    }
    submit() {
        console.log('[ Form ] submit', this.el);

        if (!this.isLoading) {
            this._beforeSubmit();
            this._processSubmit();
            this._afterSubmit();
        }
    }
    _initFields() {
        this.$fields.filter(this.opt.fieldNamePrefix + 'captcha').remove();
    }
    _initEvents() {
        this.$form.on('submit', (e) => {
            e.preventDefault();
            this.submit();
        });
    }
    _beforeSubmit() {
        this.isLoading = true;

        this.$fields.removeClass(this.opt.errorClass);

        $.fancybox.showLoading();
    }
    _processSubmit() {
        let params = new FormData(this.el);

        Ajax.get('send_form', params).then((data) => {
            if (data.ok) {
                console.log('[ Form/_processSubmit ] Sent successfully');

                if (this.$form.data('goal')) {
                    $(window).trigger('goal.main', this.$form.data());
                }

                if (this.$content.length) {
                    this.$content.html(data.content);
                    this.$form.remove();
                } else {
                    this.$form.replaceWith(data.content);
                }
                this._afterSubmit();
            } else if (data.errors) {
                console.log('[ Form/_processSubmit ] Got errors', data.errors);

                for (let p in data.errors) {
                    this.$fields.filter(this.opt.fieldNamePrefix + p).addClass(this.opt.errorClass);
                }
                this._afterSubmit();
            }
        }).catch(function(error) {
            console.error('[ Form/_processSubmit ] Sending failed', error);

            this._afterSubmit();
        });
    }
    _afterSubmit() {
        $.fancybox.hideLoading();

        if ($.fancybox.isOpen) {
            $.fancybox.update();
        }

        this.isLoading = false;
    }
}


export {
    Form
};
