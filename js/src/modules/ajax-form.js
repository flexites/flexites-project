'use strict';

/* global $ */

import { Ajax } from 'utils/ajax';
import { fancybox } from 'settings';

import { Form } from './ajax-form/form';


let settings = {
    formSelector: '.js-ajax-form__form',
    linkSelector: '.js-ajax-form__link',
    fancybox: {},
    form: {},
    key: window.Symbol ? window.Symbol('ajax-form') : '__ajax_form'
};


class AjaxForm {
    constructor(options) {
        this.opt = $.extend(true, {}, settings, options);
        this.init();
    }
    init() {
        console.log('[ AjaxForm ] init');

        let self = this;

        $(document.body).on('click', this.opt.linkSelector, function(e) {
            e.preventDefault();

            let $el = $(this);
            self.getForm($.extend({}, { href: $el.attr('href') }, $el.data()));
        });

        $(this.opt.formSelector).each(function() {
            if (!this[ self.opt.key ]) {
                this[ self.opt.key ] = new Form(this, self.opt.form);
            }
        });

        $(window).on('initAjaxForm.main', () => {
            $(this.opt.formSelector).each(function() {
                if (!this[ self.opt.key ]) {
                    this[ self.opt.key ] = new Form(this, self.opt.form);
                }
            });
        });
    }
    getForm(params) {
        console.log('[ AjaxForm ] getForm', params);

        Ajax.get('get_form', params).then((data) => {
            if (data.ok) {
                this.showForm(data.content);
            } else {
                console.error('Can\'t get form');
            }
        }).catch(function(error) {
            throw new Error(error);
        });
    }
    showForm(content) {
        console.log('[ AjaxForm ] showForm');

        let self = this;

        $.fancybox($.extend(true, {}, fancybox, this.opt.fancybox, {
            type: 'html',
            content: content,
            afterShow: function() {
                let form = this.inner.find(self.opt.formSelector).get(0);
                form[ self.opt.key ] = new Form(form, self.opt.form);
            }
        }));
    }
}


export {
    AjaxForm
};
