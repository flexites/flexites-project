'use strict';

/* global $ */

import { counters } from 'utils/counters';


class Goal {
    constructor(options) {
        this.opt = $.extend({}, {
            goals: [],
            counters: counters
        }, options);
        this.init();
    }
    init() {
        console.log('[ Goal ] init');

        for (let i = 0; i < this.opt.goals.length; i ++) {
            this.bind(this.opt.goals[i]);
        }
        $(window).on('goal.main', (e, data) => {
            this.reach(data.goal, data.params);
        });
    }
    bind(goal) {
        console.log('[ Goal ] bind', goal);

        let handler = (e, data) => {
            if (goal.goal) {
                this.reach(goal.goal, goal.params);
            } else if (goal.handler) {
                goal.handler.call(e.target, e, data);
            } else {
                throw new Error('Neither goal nor handler are defined');
            }
        };

        if (this.__selectorIsDelegator(goal.selector)) {
            this.$body.on(goal.event, goal.selector, handler);
        } else {
            $(goal.selector).on(goal.event, handler);
        }
    }
    reach(goal, params) {
        console.log('[ Goal ] reach', goal);

        for (let i = 0; i < this.opt.counters.length; i ++) {
            this.opt.counters[i].reachGoal(goal, params);
        }
    }
    get $body() {
        if (!this.__$body) {
            this.__$body = $(document.body);
        }
        return this.__$body;
    }
    __selectorIsDelegator(selector) {
        return [window, document, document.documentElement, document.body, 'html', 'body'].indexOf(selector) === -1;
    }
}


export {
    Goal
};
