-- MySQL dump 10.15  Distrib 10.0.34-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: _projectName_
-- ------------------------------------------------------
-- Server version	10.0.34-MariaDB-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `T_##_CAT_CLASSES`
--

DROP TABLE IF EXISTS `T_##_CAT_CLASSES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_##_CAT_CLASSES` (
  `name` varchar(100) NOT NULL DEFAULT '',
  `descr` mediumtext NOT NULL,
  `interface_name` varchar(100) DEFAULT NULL,
  `in_site_map` tinyint(1) NOT NULL DEFAULT '0',
  `gallery` tinyint(1) DEFAULT '0',
  `cat_id` int(4) DEFAULT '0',
  PRIMARY KEY (`name`),
  KEY `site_map_idx` (`in_site_map`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_##_CAT_CLASSES`
--

LOCK TABLES `T_##_CAT_CLASSES` WRITE;
/*!40000 ALTER TABLE `T_##_CAT_CLASSES` DISABLE KEYS */;
INSERT INTO `T_##_CAT_CLASSES` VALUES ('auxiliary_cat','Дополнительные разделы сайта',NULL,0,0,0),('banners_manager','Управление баннерами',NULL,0,0,0),('banners_place','Баннерное место',NULL,0,0,0),('banner_with_code','Баннер с кодом',NULL,0,0,0),('banner_with_link','Баннер со ссылкой',NULL,0,0,0),('banner_with_multimedia','Баннер с визуальным редактором',NULL,0,0,0),('form_mail','Форма. Написать письмо','',0,0,NULL),('home_page','Главная страница',NULL,0,0,0),('information_cat','Информационные разделы сайта','',0,0,NULL),('menu','Меню сайта',NULL,0,0,0),('menu_item','Пункт меню',NULL,0,0,0),('menu_place','Место меню','',0,0,0),('page','Страница',NULL,0,0,0),('page_with_form','Страница с формой',NULL,0,0,0),('root','root','',0,0,0);
/*!40000 ALTER TABLE `T_##_CAT_CLASSES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `T_##_CAT_CLASSES_ATTRS`
--

DROP TABLE IF EXISTS `T_##_CAT_CLASSES_ATTRS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_##_CAT_CLASSES_ATTRS` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `class_name` varchar(100) NOT NULL DEFAULT '',
  `name` varchar(100) NOT NULL DEFAULT '',
  `publ_name` varchar(100) NOT NULL DEFAULT '',
  `publ_comment` varchar(255) NOT NULL DEFAULT '',
  `field_type` enum('date_time','date','short_text','long_text','int','double','enum','set','bool','blob','multimedia','array','pointer','image','file','url','e_mail') DEFAULT NULL,
  `field_order` int(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `class_name_name_idx` (`class_name`,`name`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_##_CAT_CLASSES_ATTRS`
--

LOCK TABLES `T_##_CAT_CLASSES_ATTRS` WRITE;
/*!40000 ALTER TABLE `T_##_CAT_CLASSES_ATTRS` DISABLE KEYS */;
INSERT INTO `T_##_CAT_CLASSES_ATTRS` VALUES (3,'home_page','content','Содержимое','','multimedia',10),(4,'home_page','position','Порядок следования','','int',20),(5,'home_page','title','Title','','short_text',40),(6,'home_page','description','Description','','short_text',50),(7,'home_page','keywords','Keywords','','short_text',60),(8,'page','content','Содержимое','','multimedia',10),(9,'page','position','Порядок следования','','int',20),(10,'page','publication','Публиковать?','','bool',30),(11,'page','title','Title','','short_text',40),(12,'page','description','Description','','short_text',50),(13,'page','keywords','Keywords','','short_text',60),(21,'menu_item','node','Узел каталога','','pointer',10),(23,'menu_item','link','Ссылка','','url',20),(24,'menu_item','position','Порядок следования','','int',30),(25,'menu_item','publication','Публиковать?','','bool',40),(26,'menu_place','place','Место','','enum',10),(27,'banner_with_code','banner_code','Код баннерной системы','','long_text',10),(28,'banner_with_code','target','Target (_blank- в новом окне)','','enum',20),(29,'banner_with_code','expiration_date','Дата окончания публикации','','date',30),(30,'banner_with_code','publication','Публиковать?','','bool',40),(31,'banner_with_link','link','Ссылка','','url',10),(32,'banner_with_link','image','Файл изображения','','file',20),(33,'banner_with_link','target','Target (_blank- в новом окне)','','enum',30),(34,'banner_with_link','expiration_date','Дата окончания публикации','','date',40),(35,'banner_with_link','publication','Публиковать?','','bool',50),(36,'banner_with_multimedia','link','Ссылка','','url',10),(37,'banner_with_multimedia','content','Содержимое','','multimedia',20),(38,'banner_with_multimedia','target','Target (_blank- в новом окне)','','enum',25),(39,'banner_with_multimedia','expiration_date','Дата окончания публикации','','date',40),(40,'banner_with_multimedia','publication','Публиковать?','','bool',50),(41,'banners_place','place','Местоположение','','enum',10),(42,'banners_place','banners_quantity','Кол-во показываемых баннеров','','int',20),(43,'home_page','publication','Публиковать?','','bool',30),(44,'banner_with_multimedia','start_date','Дата начала публикации','','date',30),(45,'banner_with_code','start_date','Дата начала публикации','','date',25),(46,'banner_with_link','start_date','Дата начала публикации','','date',35),(47,'page_with_form','publication','Публиковать?','','bool',110),(48,'page_with_form','_form_class_name','Имя класса формы','','short_text',20),(49,'page_with_form','_recipient_mail_address','E-mail адрес получателя','','short_text',30),(50,'page_with_form','_mail_template','Шаблон для письма (пустое поле - стандартный шаблон)','','short_text',40),(51,'page_with_form','_mail_subject','Тема письма','','short_text',50),(52,'page_with_form','_mail_encoding','Кодировка письма','','enum',60),(53,'page_with_form','_content','Содержание','','multimedia',70),(54,'page_with_form','_s_content','Сообщение об успешной отправке','','multimedia',80),(55,'page_with_form','_create_container','Создавать контейнер?','','bool',90),(56,'page_with_form','position','Порядок следования','','int',100),(57,'page_with_form','title','Title','','short_text',120),(58,'page_with_form','keywords','Keywords','','short_text',140),(59,'page_with_form','description','Description','','short_text',130),(60,'form_mail','addition_date','Дата добавления','','date',10),(61,'form_mail','content','Текст сообщения','','long_text',20),(62,'form_mail','person','Ваше имя','','short_text',30),(63,'form_mail','phone','Телефон','','short_text',40),(64,'form_mail','email','E-mail для ответа (обратный адрес)','','e_mail',50),(65,'page_with_form','goal','Цель при отправке формы','','short_text',10);
/*!40000 ALTER TABLE `T_##_CAT_CLASSES_ATTRS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `T_##_CAT_CLASSES_ATTRS_CHECKS`
--

DROP TABLE IF EXISTS `T_##_CAT_CLASSES_ATTRS_CHECKS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_##_CAT_CLASSES_ATTRS_CHECKS` (
  `check_nick` varchar(50) NOT NULL DEFAULT '',
  `check_descr` varchar(255) NOT NULL DEFAULT '',
  KEY `check_nick_idx` (`check_nick`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_##_CAT_CLASSES_ATTRS_CHECKS`
--

LOCK TABLES `T_##_CAT_CLASSES_ATTRS_CHECKS` WRITE;
/*!40000 ALTER TABLE `T_##_CAT_CLASSES_ATTRS_CHECKS` DISABLE KEYS */;
INSERT INTO `T_##_CAT_CLASSES_ATTRS_CHECKS` VALUES ('CheckEmail','email'),('CheckPhone','телефон'),('CheckNumber','целое число'),('CheckDouble','нецелое число'),('CheckNull','обязательное'),('CheckFileFormatImg','изображение'),('CheckFileFormatFlash','флэш'),('CheckDate','дата'),('CheckDateTime','дата, время'),('CheckTime','время'),('CheckHTML','безопасный html'),('AddHTTPtoURL','добавлять http:// к адресам');
/*!40000 ALTER TABLE `T_##_CAT_CLASSES_ATTRS_CHECKS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `T_##_CAT_CLASSES_ATTRS_CHECKS_REL`
--

DROP TABLE IF EXISTS `T_##_CAT_CLASSES_ATTRS_CHECKS_REL`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_##_CAT_CLASSES_ATTRS_CHECKS_REL` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `class_name` varchar(100) NOT NULL DEFAULT '',
  `attr_id` int(8) NOT NULL DEFAULT '0',
  `check_type` enum('standart','regexp','action') NOT NULL DEFAULT 'standart',
  `check_nick` varchar(50) NOT NULL DEFAULT '',
  `check_descr` varchar(255) NOT NULL DEFAULT '',
  `check_regexp` varchar(255) NOT NULL DEFAULT '',
  `check_module` varchar(200) NOT NULL DEFAULT '',
  `check_class` varchar(200) NOT NULL DEFAULT '',
  `check_method` varchar(200) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `attr_id_idx` (`attr_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_##_CAT_CLASSES_ATTRS_CHECKS_REL`
--

LOCK TABLES `T_##_CAT_CLASSES_ATTRS_CHECKS_REL` WRITE;
/*!40000 ALTER TABLE `T_##_CAT_CLASSES_ATTRS_CHECKS_REL` DISABLE KEYS */;
INSERT INTO `T_##_CAT_CLASSES_ATTRS_CHECKS_REL` VALUES (1,'form_mail',61,'standart','CheckNull','обязательное','','','',''),(2,'form_mail',62,'standart','CheckNull','обязательное','','','','');
/*!40000 ALTER TABLE `T_##_CAT_CLASSES_ATTRS_CHECKS_REL` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `T_##_CAT_CLASSES_ATTRS_GROUPS`
--

DROP TABLE IF EXISTS `T_##_CAT_CLASSES_ATTRS_GROUPS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_##_CAT_CLASSES_ATTRS_GROUPS` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `class_name` varchar(100) NOT NULL DEFAULT '',
  `group_name` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `class_name_idx` (`class_name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_##_CAT_CLASSES_ATTRS_GROUPS`
--

LOCK TABLES `T_##_CAT_CLASSES_ATTRS_GROUPS` WRITE;
/*!40000 ALTER TABLE `T_##_CAT_CLASSES_ATTRS_GROUPS` DISABLE KEYS */;
INSERT INTO `T_##_CAT_CLASSES_ATTRS_GROUPS` VALUES (1,'form_mail','display');
/*!40000 ALTER TABLE `T_##_CAT_CLASSES_ATTRS_GROUPS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `T_##_CAT_CLASSES_ATTRS_GROUPS_REL`
--

DROP TABLE IF EXISTS `T_##_CAT_CLASSES_ATTRS_GROUPS_REL`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_##_CAT_CLASSES_ATTRS_GROUPS_REL` (
  `group_id` int(8) NOT NULL DEFAULT '0',
  `attr_id` int(8) NOT NULL DEFAULT '0',
  KEY `idx_1` (`group_id`,`attr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_##_CAT_CLASSES_ATTRS_GROUPS_REL`
--

LOCK TABLES `T_##_CAT_CLASSES_ATTRS_GROUPS_REL` WRITE;
/*!40000 ALTER TABLE `T_##_CAT_CLASSES_ATTRS_GROUPS_REL` DISABLE KEYS */;
INSERT INTO `T_##_CAT_CLASSES_ATTRS_GROUPS_REL` VALUES (1,61),(1,62),(1,63),(1,64);
/*!40000 ALTER TABLE `T_##_CAT_CLASSES_ATTRS_GROUPS_REL` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `T_##_CAT_CLASSES_AVAIL_CLASSES`
--

DROP TABLE IF EXISTS `T_##_CAT_CLASSES_AVAIL_CLASSES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_##_CAT_CLASSES_AVAIL_CLASSES` (
  `class_name` varchar(100) NOT NULL DEFAULT '',
  `avail_class` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`class_name`,`avail_class`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_##_CAT_CLASSES_AVAIL_CLASSES`
--

LOCK TABLES `T_##_CAT_CLASSES_AVAIL_CLASSES` WRITE;
/*!40000 ALTER TABLE `T_##_CAT_CLASSES_AVAIL_CLASSES` DISABLE KEYS */;
INSERT INTO `T_##_CAT_CLASSES_AVAIL_CLASSES` VALUES ('banners_manager','banners_place'),('banners_place','banner_with_code'),('banners_place','banner_with_link'),('banners_place','banner_with_multimedia'),('information_cat','home_page'),('information_cat','page'),('information_cat','page_with_form'),('menu','menu_place'),('menu_place','menu_item'),('page','page'),('root','auxiliary_cat'),('root','banners_manager'),('root','information_cat'),('root','menu');
/*!40000 ALTER TABLE `T_##_CAT_CLASSES_AVAIL_CLASSES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `T_##_CAT_CPATHS`
--

DROP TABLE IF EXISTS `T_##_CAT_CPATHS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_##_CAT_CPATHS` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `nick` varchar(100) NOT NULL DEFAULT '',
  `context` int(8) NOT NULL DEFAULT '0',
  `ord` int(4) NOT NULL DEFAULT '0',
  `class_name` varchar(100) NOT NULL DEFAULT '',
  `attr_name` varchar(100) NOT NULL DEFAULT '',
  `attr_value` varchar(255) NOT NULL DEFAULT '',
  `all_tree` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `nick_idx` (`nick`),
  KEY `ord_idx` (`ord`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_##_CAT_CPATHS`
--

LOCK TABLES `T_##_CAT_CPATHS` WRITE;
/*!40000 ALTER TABLE `T_##_CAT_CPATHS` DISABLE KEYS */;
INSERT INTO `T_##_CAT_CPATHS` VALUES (4,'menu_item.node',4,1,'home_page','','',1),(5,'menu_item.node',4,2,'page','','',1),(6,'menu_item.node',4,3,'page_with_form','','',1);
/*!40000 ALTER TABLE `T_##_CAT_CPATHS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `T_##_CAT_ENUMS`
--

DROP TABLE IF EXISTS `T_##_CAT_ENUMS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_##_CAT_ENUMS` (
  `attr_id` int(8) NOT NULL DEFAULT '0',
  `val` varchar(255) NOT NULL DEFAULT '0',
  `pos` int(8) DEFAULT '0',
  PRIMARY KEY (`attr_id`,`val`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_##_CAT_ENUMS`
--

LOCK TABLES `T_##_CAT_ENUMS` WRITE;
/*!40000 ALTER TABLE `T_##_CAT_ENUMS` DISABLE KEYS */;
INSERT INTO `T_##_CAT_ENUMS` VALUES (26,'1',0),(26,'2',0),(26,'3',0),(28,'_blank',0),(28,'_self',0),(33,'_blank',0),(33,'_self',0),(38,'_blank',0),(38,'_self',0),(41,'1',0),(41,'2',0),(41,'3',0),(41,'4',0),(41,'5',0),(52,'cp1251',0),(52,'koi8-r',0),(52,'utf8',0);
/*!40000 ALTER TABLE `T_##_CAT_ENUMS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `T_##_CAT_EVENTS`
--

DROP TABLE IF EXISTS `T_##_CAT_EVENTS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_##_CAT_EVENTS` (
  `object` varchar(100) NOT NULL DEFAULT '',
  `event` varchar(100) NOT NULL DEFAULT '',
  `method` varchar(100) NOT NULL DEFAULT '',
  KEY `event_object` (`event`,`object`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_##_CAT_EVENTS`
--

LOCK TABLES `T_##_CAT_EVENTS` WRITE;
/*!40000 ALTER TABLE `T_##_CAT_EVENTS` DISABLE KEYS */;
/*!40000 ALTER TABLE `T_##_CAT_EVENTS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `T_##_CAT_GALLERY`
--

DROP TABLE IF EXISTS `T_##_CAT_GALLERY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_##_CAT_GALLERY` (
  `photo_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `node_id` int(8) NOT NULL,
  `file_id` int(4) NOT NULL,
  `photo_name` varchar(255) DEFAULT NULL,
  `description` text,
  `position` int(4) DEFAULT '0',
  `publication` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`photo_id`),
  KEY `gallery_node_id_fk` (`node_id`),
  KEY `gallery_file_id_fk` (`file_id`),
  KEY `gallery_position_idx` (`position`),
  KEY `gallery_publication_idx` (`publication`),
  CONSTRAINT `gallery_file_id_fk` FOREIGN KEY (`file_id`) REFERENCES `T_##_FILES` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `gallery_node_id_fk` FOREIGN KEY (`node_id`) REFERENCES `T_##_CAT_NODES` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_##_CAT_GALLERY`
--

LOCK TABLES `T_##_CAT_GALLERY` WRITE;
/*!40000 ALTER TABLE `T_##_CAT_GALLERY` DISABLE KEYS */;
/*!40000 ALTER TABLE `T_##_CAT_GALLERY` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `T_##_CAT_HEAP_DATE`
--

DROP TABLE IF EXISTS `T_##_CAT_HEAP_DATE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_##_CAT_HEAP_DATE` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `node_id` int(8) NOT NULL DEFAULT '0',
  `attr_name` varchar(100) NOT NULL DEFAULT '',
  `class_name` varchar(100) NOT NULL DEFAULT '',
  `value` datetime NOT NULL DEFAULT '0001-01-01 00:00:00',
  PRIMARY KEY (`id`),
  KEY `node_attr_value_class_idx` (`node_id`,`attr_name`,`value`,`class_name`),
  KEY `attr_value` (`attr_name`,`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_##_CAT_HEAP_DATE`
--

LOCK TABLES `T_##_CAT_HEAP_DATE` WRITE;
/*!40000 ALTER TABLE `T_##_CAT_HEAP_DATE` DISABLE KEYS */;
/*!40000 ALTER TABLE `T_##_CAT_HEAP_DATE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `T_##_CAT_HEAP_DOUBLE`
--

DROP TABLE IF EXISTS `T_##_CAT_HEAP_DOUBLE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_##_CAT_HEAP_DOUBLE` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `node_id` int(8) NOT NULL DEFAULT '0',
  `attr_name` varchar(100) NOT NULL DEFAULT '',
  `class_name` varchar(100) NOT NULL DEFAULT '',
  `value` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `node_attr_value_class_idx` (`node_id`,`attr_name`,`value`,`class_name`),
  KEY `attr_value` (`attr_name`,`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_##_CAT_HEAP_DOUBLE`
--

LOCK TABLES `T_##_CAT_HEAP_DOUBLE` WRITE;
/*!40000 ALTER TABLE `T_##_CAT_HEAP_DOUBLE` DISABLE KEYS */;
/*!40000 ALTER TABLE `T_##_CAT_HEAP_DOUBLE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `T_##_CAT_HEAP_INT`
--

DROP TABLE IF EXISTS `T_##_CAT_HEAP_INT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_##_CAT_HEAP_INT` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `node_id` int(8) NOT NULL DEFAULT '0',
  `attr_name` varchar(100) NOT NULL DEFAULT '',
  `class_name` varchar(100) NOT NULL DEFAULT '',
  `value` int(8) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `node_attr_value_class_idx` (`node_id`,`attr_name`,`value`,`class_name`),
  KEY `attr_value` (`attr_name`,`value`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_##_CAT_HEAP_INT`
--

LOCK TABLES `T_##_CAT_HEAP_INT` WRITE;
/*!40000 ALTER TABLE `T_##_CAT_HEAP_INT` DISABLE KEYS */;
INSERT INTO `T_##_CAT_HEAP_INT` VALUES (42,6,'position','home_page',10),(43,6,'publication','home_page',1),(36,7,'position','page',20),(37,7,'publication','page',1),(20,9,'position','menu_item',20),(14,9,'publication','menu_item',1),(19,10,'position','menu_item',10),(18,10,'publication','menu_item',1),(47,11,'position','page_with_form',30),(48,11,'publication','page_with_form',1),(46,11,'_create_container','page_with_form',0),(29,12,'position','menu_item',30),(30,12,'publication','menu_item',1),(45,13,'banners_quantity','banners_place',99);
/*!40000 ALTER TABLE `T_##_CAT_HEAP_INT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `T_##_CAT_HEAP_POINTER`
--

DROP TABLE IF EXISTS `T_##_CAT_HEAP_POINTER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_##_CAT_HEAP_POINTER` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `node_id` int(8) NOT NULL DEFAULT '0',
  `attr_name` varchar(100) NOT NULL DEFAULT '',
  `class_name` varchar(100) NOT NULL DEFAULT '',
  `value` int(8) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `node_attr_value_class_idx` (`node_id`,`attr_name`,`value`,`class_name`),
  KEY `attr_value` (`attr_name`,`value`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_##_CAT_HEAP_POINTER`
--

LOCK TABLES `T_##_CAT_HEAP_POINTER` WRITE;
/*!40000 ALTER TABLE `T_##_CAT_HEAP_POINTER` DISABLE KEYS */;
INSERT INTO `T_##_CAT_HEAP_POINTER` VALUES (4,9,'node','menu_item',7),(6,10,'node','menu_item',6),(8,12,'node','menu_item',11);
/*!40000 ALTER TABLE `T_##_CAT_HEAP_POINTER` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `T_##_CAT_HEAP_TEXT`
--

DROP TABLE IF EXISTS `T_##_CAT_HEAP_TEXT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_##_CAT_HEAP_TEXT` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `node_id` int(8) NOT NULL DEFAULT '0',
  `attr_name` varchar(100) NOT NULL DEFAULT '',
  `class_name` varchar(100) NOT NULL DEFAULT '',
  `value` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `node_attr_class_idx` (`node_id`,`attr_name`,`class_name`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_##_CAT_HEAP_TEXT`
--

LOCK TABLES `T_##_CAT_HEAP_TEXT` WRITE;
/*!40000 ALTER TABLE `T_##_CAT_HEAP_TEXT` DISABLE KEYS */;
INSERT INTO `T_##_CAT_HEAP_TEXT` VALUES (12,7,'content','page','<h2>Это текст</h2>\r\n<p>Текст на странице</p>\r\n<p>В несколько строчек</p>\r\n<h3>С подзаголовком</h3>\r\n<p>И еще немного текста</p>\r\n<p>Со <a href=\"#\">ссылкой</a></p>\r\n<h2>Это маркированный список</h2>\r\n<ul>\r\n<li>Раз</li>\r\n<li>Два</li>\r\n<li>Три</li>\r\n</ul>\r\n<h2>Это нумерованный список</h2>\r\n<ol>\r\n<li>Раз</li>\r\n<li>Два</li>\r\n<li>Три</li>\r\n</ol>\r\n<h2>Это таблица</h2>\r\n<table border=\"1\">\r\n<tbody>\r\n<tr>\r\n<td>1</td>\r\n<td>2</td>\r\n<td>3</td>\r\n</tr>\r\n<tr>\r\n<td>4</td>\r\n<td>5</td>\r\n<td>6</td>\r\n</tr>\r\n<tr>\r\n<td>7</td>\r\n<td>8</td>\r\n<td>9</td>\r\n</tr>\r\n<tr>\r\n<td>10</td>\r\n<td>11</td>\r\n<td>12</td>\r\n</tr>\r\n<tr>\r\n<td>13</td>\r\n<td>14</td>\r\n<td>15</td>\r\n</tr>\r\n</tbody>\r\n</table>'),(15,6,'content','home_page','<p>Это текст на главной странице</p>'),(16,11,'_content','page_with_form','<p>Заполните форму</p>'),(17,11,'_s_content','page_with_form','<p>Спасибо</p>');
/*!40000 ALTER TABLE `T_##_CAT_HEAP_TEXT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `T_##_CAT_HEAP_VARCHAR`
--

DROP TABLE IF EXISTS `T_##_CAT_HEAP_VARCHAR`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_##_CAT_HEAP_VARCHAR` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `node_id` int(8) NOT NULL DEFAULT '0',
  `attr_name` varchar(100) NOT NULL DEFAULT '',
  `class_name` varchar(100) NOT NULL DEFAULT '',
  `value` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `node_attr_value_class_idx` (`node_id`,`attr_name`,`value`,`class_name`),
  KEY `attr_value` (`attr_name`,`value`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_##_CAT_HEAP_VARCHAR`
--

LOCK TABLES `T_##_CAT_HEAP_VARCHAR` WRITE;
/*!40000 ALTER TABLE `T_##_CAT_HEAP_VARCHAR` DISABLE KEYS */;
INSERT INTO `T_##_CAT_HEAP_VARCHAR` VALUES (61,6,'description','home_page',''),(62,6,'keywords','home_page',''),(60,6,'title','home_page',''),(49,7,'description','page',''),(50,7,'keywords','page',''),(48,7,'title','page',''),(14,8,'place','menu_place','1'),(16,9,'link','menu_item',''),(18,10,'link','menu_item',''),(72,11,'description','page_with_form',''),(65,11,'goal','page_with_form','test'),(73,11,'keywords','page_with_form',''),(71,11,'title','page_with_form',''),(66,11,'_form_class_name','page_with_form','form_mail'),(70,11,'_mail_encoding','page_with_form','cp1251'),(69,11,'_mail_subject','page_with_form','Новое сообщение'),(68,11,'_mail_template','page_with_form',''),(67,11,'_recipient_mail_address','page_with_form','info@flexites.org'),(36,12,'link','menu_item',''),(64,13,'place','banners_place','1');
/*!40000 ALTER TABLE `T_##_CAT_HEAP_VARCHAR` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `T_##_CAT_NODES`
--

DROP TABLE IF EXISTS `T_##_CAT_NODES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_##_CAT_NODES` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `parent_id` int(8) NOT NULL DEFAULT '0',
  `class_name` varchar(100) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `onec_id` int(8) NOT NULL DEFAULT '0',
  `last_update` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lft` int(8) NOT NULL DEFAULT '0',
  `rgt` int(8) NOT NULL DEFAULT '0',
  `flag` int(4) NOT NULL DEFAULT '0',
  `slink` varchar(255) NOT NULL DEFAULT '',
  `hits` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `rgt_idx` (`rgt`),
  KEY `flag_idx` (`flag`),
  KEY `lft_rgt_class_idx` (`lft`,`rgt`,`class_name`),
  KEY `class_parent_idx` (`class_name`,`parent_id`),
  KEY `last_upd_idx` (`last_update`),
  KEY `lft_idx` (`lft`),
  KEY `slink_idx` (`slink`),
  KEY `lft_rgt_idx` (`lft`,`rgt`),
  KEY `lft_rgt_slink_idx` (`lft`,`rgt`,`slink`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_##_CAT_NODES`
--

LOCK TABLES `T_##_CAT_NODES` WRITE;
/*!40000 ALTER TABLE `T_##_CAT_NODES` DISABLE KEYS */;
INSERT INTO `T_##_CAT_NODES` VALUES (1,0,'root','root',0,'2013-02-07 11:33:32',1,26,0,'',0),(2,1,'auxiliary_cat','Вспомогательные разделы',0,'2013-02-07 11:33:32',2,3,0,'',0),(3,1,'banners_manager','Система управления баннерами',0,'2013-02-07 11:33:32',4,7,0,'',0),(4,1,'information_cat','Информационные разделы',0,'2013-02-07 11:33:32',8,15,0,'',0),(5,1,'menu','Меню',0,'2013-02-07 11:33:32',16,25,0,'',0),(6,4,'home_page','Главная страница',0,'2018-10-08 13:12:39',9,10,0,'',162),(7,4,'page','Страница',0,'2018-10-08 13:10:35',11,12,0,'page',43),(8,5,'menu_place','1 Главное меню',0,'2018-09-26 15:24:44',17,24,0,'',0),(9,8,'menu_item','Страница',0,'2018-09-26 15:24:56',18,19,0,'',0),(10,8,'menu_item','Главная',0,'2018-09-26 15:25:13',20,21,0,'',0),(11,4,'page_with_form','Страница с формой',0,'2018-10-10 08:47:14',13,14,0,'page-with-form',285),(12,8,'menu_item','Страница с формой',0,'2018-09-26 15:41:01',22,23,0,'',0),(13,3,'banners_place','1 Счетчики',0,'2018-10-08 13:29:12',5,6,0,'',0);
/*!40000 ALTER TABLE `T_##_CAT_NODES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `T_##_CAT_NODES_EXCEPTS`
--

DROP TABLE IF EXISTS `T_##_CAT_NODES_EXCEPTS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_##_CAT_NODES_EXCEPTS` (
  `object_id` int(8) NOT NULL,
  `update_freq` varchar(100) NOT NULL,
  `priority` double NOT NULL DEFAULT '0.5',
  PRIMARY KEY (`object_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_##_CAT_NODES_EXCEPTS`
--

LOCK TABLES `T_##_CAT_NODES_EXCEPTS` WRITE;
/*!40000 ALTER TABLE `T_##_CAT_NODES_EXCEPTS` DISABLE KEYS */;
/*!40000 ALTER TABLE `T_##_CAT_NODES_EXCEPTS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `T_##_CAT_NODES_TEMP`
--

DROP TABLE IF EXISTS `T_##_CAT_NODES_TEMP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_##_CAT_NODES_TEMP` (
  `id` int(8) NOT NULL DEFAULT '0',
  `object_id` int(8) NOT NULL DEFAULT '0',
  `class_name` varchar(100) NOT NULL DEFAULT '',
  `object_name` varchar(255) NOT NULL DEFAULT '',
  `search_hash` int(6) unsigned DEFAULT '0',
  `session_hash` varchar(36) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_##_CAT_NODES_TEMP`
--

LOCK TABLES `T_##_CAT_NODES_TEMP` WRITE;
/*!40000 ALTER TABLE `T_##_CAT_NODES_TEMP` DISABLE KEYS */;
/*!40000 ALTER TABLE `T_##_CAT_NODES_TEMP` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `T_##_COOKIES`
--

DROP TABLE IF EXISTS `T_##_COOKIES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_##_COOKIES` (
  `client_id` varchar(36) NOT NULL DEFAULT '',
  `uid` int(8) DEFAULT NULL,
  `vars` blob,
  `last_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`client_id`),
  KEY `lt_idx` (`last_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_##_COOKIES`
--

LOCK TABLES `T_##_COOKIES` WRITE;
/*!40000 ALTER TABLE `T_##_COOKIES` DISABLE KEYS */;
/*!40000 ALTER TABLE `T_##_COOKIES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `T_##_EVENTS`
--

DROP TABLE IF EXISTS `T_##_EVENTS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_##_EVENTS` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `module` varchar(100) NOT NULL DEFAULT '',
  `object` varchar(100) NOT NULL DEFAULT '',
  `event` varchar(100) NOT NULL DEFAULT '',
  `handler_module` varchar(200) NOT NULL DEFAULT '',
  `handler_class` varchar(200) NOT NULL DEFAULT '',
  `handler_method` varchar(200) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `module_idx` (`module`),
  KEY `object_idx` (`object`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_##_EVENTS`
--

LOCK TABLES `T_##_EVENTS` WRITE;
/*!40000 ALTER TABLE `T_##_EVENTS` DISABLE KEYS */;
INSERT INTO `T_##_EVENTS` VALUES (1,'Secure','XCat.Object','SetRights','XCatNew','actions::XCatSecure','RecursiveGiveRights'),(2,'XCatNew','XCat.Object','Create','XCatNew','actions::XCatSecure','OnCreateObjectGiveRights'),(3,'XCatNew','XCat.Object','Delete','XCatNew','actions::XCatSecure','OnDeleteObjectRemoveRights'),(4,'XCatNew','XCat.Object','Move','XCatNew','actions::XCatSecure','OnMoveObjectSetRights');
/*!40000 ALTER TABLE `T_##_EVENTS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `T_##_FILES`
--

DROP TABLE IF EXISTS `T_##_FILES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_##_FILES` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `cat_id` int(4) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT '',
  `descr` mediumtext,
  `file_name` varchar(255) NOT NULL DEFAULT '',
  `size` double NOT NULL DEFAULT '0',
  `type` varchar(20) NOT NULL DEFAULT '',
  `relative_path` varchar(255) NOT NULL DEFAULT '',
  `full_path` varchar(255) NOT NULL DEFAULT '',
  `in_trash` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `cat_id_idx` (`cat_id`),
  KEY `type_idx` (`type`),
  KEY `file_in_trash_idx` (`in_trash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_##_FILES`
--

LOCK TABLES `T_##_FILES` WRITE;
/*!40000 ALTER TABLE `T_##_FILES` DISABLE KEYS */;
/*!40000 ALTER TABLE `T_##_FILES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `T_##_FILES_CAT`
--

DROP TABLE IF EXISTS `T_##_FILES_CAT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_##_FILES_CAT` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `parent_id` int(4) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT '',
  `descr` mediumtext,
  `in_trash` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `cat_in_trash_idx` (`in_trash`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_##_FILES_CAT`
--

LOCK TABLES `T_##_FILES_CAT` WRITE;
/*!40000 ALTER TABLE `T_##_FILES_CAT` DISABLE KEYS */;
INSERT INTO `T_##_FILES_CAT` VALUES (1,-1,'trash','Корзина',0);
/*!40000 ALTER TABLE `T_##_FILES_CAT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `T_##_FILES_FLASH_PARAMS`
--

DROP TABLE IF EXISTS `T_##_FILES_FLASH_PARAMS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_##_FILES_FLASH_PARAMS` (
  `file_id` int(4) NOT NULL DEFAULT '0',
  `size_x` int(4) NOT NULL DEFAULT '0',
  `size_y` int(4) NOT NULL DEFAULT '0',
  `background` char(7) NOT NULL DEFAULT '#ffffff',
  `version` int(4) NOT NULL DEFAULT '8',
  PRIMARY KEY (`file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_##_FILES_FLASH_PARAMS`
--

LOCK TABLES `T_##_FILES_FLASH_PARAMS` WRITE;
/*!40000 ALTER TABLE `T_##_FILES_FLASH_PARAMS` DISABLE KEYS */;
/*!40000 ALTER TABLE `T_##_FILES_FLASH_PARAMS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `T_##_FILES_IMG_PARAMS`
--

DROP TABLE IF EXISTS `T_##_FILES_IMG_PARAMS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_##_FILES_IMG_PARAMS` (
  `file_id` int(4) NOT NULL DEFAULT '0',
  `size_x` int(4) NOT NULL DEFAULT '0',
  `size_y` int(4) NOT NULL DEFAULT '0',
  `small_size_x` int(4) NOT NULL DEFAULT '0',
  `small_size_y` int(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_##_FILES_IMG_PARAMS`
--

LOCK TABLES `T_##_FILES_IMG_PARAMS` WRITE;
/*!40000 ALTER TABLE `T_##_FILES_IMG_PARAMS` DISABLE KEYS */;
/*!40000 ALTER TABLE `T_##_FILES_IMG_PARAMS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `T_##_HEAD_TAGS`
--

DROP TABLE IF EXISTS `T_##_HEAD_TAGS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_##_HEAD_TAGS` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  `value` varchar(350) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_##_HEAD_TAGS`
--

LOCK TABLES `T_##_HEAD_TAGS` WRITE;
/*!40000 ALTER TABLE `T_##_HEAD_TAGS` DISABLE KEYS */;
INSERT INTO `T_##_HEAD_TAGS` VALUES (1,'Google verification',''),(2,'Yandex verification','');
/*!40000 ALTER TABLE `T_##_HEAD_TAGS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `T_##_HTTP_SESSION`
--

DROP TABLE IF EXISTS `T_##_HTTP_SESSION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_##_HTTP_SESSION` (
  `session_id` varchar(36) NOT NULL DEFAULT '',
  `uid` int(8) NOT NULL DEFAULT '0',
  `vars` blob,
  `last_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ip` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`session_id`),
  KEY `uid` (`uid`),
  KEY `session_last_time` (`last_time`),
  KEY `session_ip` (`ip`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_##_HTTP_SESSION`
--

LOCK TABLES `T_##_HTTP_SESSION` WRITE;
/*!40000 ALTER TABLE `T_##_HTTP_SESSION` DISABLE KEYS */;
/*!40000 ALTER TABLE `T_##_HTTP_SESSION` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `T_##_SECURITY_ENTITIES`
--

DROP TABLE IF EXISTS `T_##_SECURITY_ENTITIES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_##_SECURITY_ENTITIES` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `entity_name` varchar(100) NOT NULL DEFAULT '',
  `descr` mediumtext,
  `prev_id` int(8) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `entity_name_idx` (`entity_name`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_##_SECURITY_ENTITIES`
--

LOCK TABLES `T_##_SECURITY_ENTITIES` WRITE;
/*!40000 ALTER TABLE `T_##_SECURITY_ENTITIES` DISABLE KEYS */;
INSERT INTO `T_##_SECURITY_ENTITIES` VALUES (1,'ALL','Любые объекты',0),(2,'Secure','Система безопаснсти',0),(3,'Settings','Системные настройки.',0),(4,'XCat.Class','Классы каталога',0),(5,'XCat.Object','Объекты каталога',0),(6,'XCat.Class.AttrGroup','Группы атрибутов',0),(7,'XCat.Archive','Архив классов',0),(8,'Files.Cat','Каталог файлов',0),(9,'Seo','Продвижение и оптимизация',0);
/*!40000 ALTER TABLE `T_##_SECURITY_ENTITIES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `T_##_SECURITY_ENTITIES_METHODS`
--

DROP TABLE IF EXISTS `T_##_SECURITY_ENTITIES_METHODS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_##_SECURITY_ENTITIES_METHODS` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `entity_type` int(8) NOT NULL DEFAULT '0',
  `method_name` varchar(100) NOT NULL DEFAULT '',
  `descr` mediumtext,
  `default_expire_period` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`id`),
  KEY `method_name_idx` (`method_name`),
  KEY `entity_type_idx` (`entity_type`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_##_SECURITY_ENTITIES_METHODS`
--

LOCK TABLES `T_##_SECURITY_ENTITIES_METHODS` WRITE;
/*!40000 ALTER TABLE `T_##_SECURITY_ENTITIES_METHODS` DISABLE KEYS */;
INSERT INTO `T_##_SECURITY_ENTITIES_METHODS` VALUES (1,1,'ALL','Любые действия','9999-01-01'),(2,2,'Manage','Доступ к управлению системой безопасности','9999-01-01'),(3,2,'NewGroup','Создание групп','9999-01-01'),(4,2,'EditGroup','Модификация групп','9999-01-01'),(5,2,'DeleteGroup','Удаление группы','9999-01-01'),(6,2,'NewLogin','Создание логинов','9999-01-01'),(7,2,'EditLogin','Модификация логинов','9999-01-01'),(8,2,'DeleteLogin','Удаление логинов','9999-01-01'),(9,2,'ManageRights','Выставление прав','9999-01-01'),(10,3,'Show','Отобразить настройки.','9999-01-01'),(11,3,'Delete','Удалить настройку.','9999-01-01'),(12,3,'Edit','Редактировать настройку.','9999-01-01'),(13,3,'Add','Добавить параметр.','9999-01-01'),(14,4,'ShowAttrs','Просмотр атрибутов','9999-01-01'),(15,4,'New','Создание класса','9999-01-01'),(16,4,'ModAttrs','Изменеие атрибутов класса','9999-01-01'),(17,4,'Edit','Модификация класса','9999-01-01'),(18,4,'Delete','Удаление класса','9999-01-01'),(19,7,'View','Просмотр архива классов','9999-01-01'),(20,5,'Show','Просмотр объекта','9999-01-01'),(21,5,'New','Создание нового объекта','9999-01-01'),(22,5,'Edit','Модификация объекта','9999-01-01'),(23,5,'Delete','Удаление объекта','9999-01-01'),(24,5,'Recurse','Флаг рекурсивности','9999-01-01'),(25,6,'Show','Просмотр группы атрибутов','9999-01-01'),(26,5,'Search','Просмотр результатов поиска','9999-01-01'),(27,8,'Show','Показать раздел','9999-01-01'),(28,8,'New','Создать раздел и закачать файл в раздел','9999-01-01'),(29,8,'Edit','Модифицировать раздел и модифицировать файлы в разделе','9999-01-01'),(30,8,'Delete','Удалить раздел и файлы в нём','9999-01-01'),(31,9,'Show','Показать список','9999-01-01');
/*!40000 ALTER TABLE `T_##_SECURITY_ENTITIES_METHODS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `T_##_SECURITY_GROUPS`
--

DROP TABLE IF EXISTS `T_##_SECURITY_GROUPS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_##_SECURITY_GROUPS` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `prev_id` int(8) NOT NULL DEFAULT '0',
  `name` varchar(100) DEFAULT NULL,
  `descr` mediumtext,
  `personal` int(1) NOT NULL DEFAULT '0',
  `level` int(4) NOT NULL DEFAULT '0',
  `empty` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `prev_id_idx` (`prev_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_##_SECURITY_GROUPS`
--

LOCK TABLES `T_##_SECURITY_GROUPS` WRITE;
/*!40000 ALTER TABLE `T_##_SECURITY_GROUPS` DISABLE KEYS */;
INSERT INTO `T_##_SECURITY_GROUPS` VALUES (1,0,'root','root',0,0,0),(2,1,'nobody','nobody\'s personal group',1,1,1),(3,1,'admin','Персональная группа admin',1,1,1),(4,1,'manager','Персональная группа manager',1,1,1);
/*!40000 ALTER TABLE `T_##_SECURITY_GROUPS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `T_##_SECURITY_GROUPS_MEMBERSHIP`
--

DROP TABLE IF EXISTS `T_##_SECURITY_GROUPS_MEMBERSHIP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_##_SECURITY_GROUPS_MEMBERSHIP` (
  `p_id` int(8) NOT NULL DEFAULT '0',
  `c_id` int(8) NOT NULL DEFAULT '0',
  `expires` date DEFAULT '9999-01-01',
  PRIMARY KEY (`p_id`,`c_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_##_SECURITY_GROUPS_MEMBERSHIP`
--

LOCK TABLES `T_##_SECURITY_GROUPS_MEMBERSHIP` WRITE;
/*!40000 ALTER TABLE `T_##_SECURITY_GROUPS_MEMBERSHIP` DISABLE KEYS */;
INSERT INTO `T_##_SECURITY_GROUPS_MEMBERSHIP` VALUES (1,3,'9999-01-01'),(2,2,'9999-01-01'),(3,3,'9999-01-01'),(4,4,'9999-01-01');
/*!40000 ALTER TABLE `T_##_SECURITY_GROUPS_MEMBERSHIP` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `T_##_SECURITY_LOGINS`
--

DROP TABLE IF EXISTS `T_##_SECURITY_LOGINS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_##_SECURITY_LOGINS` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `login` varchar(255) NOT NULL DEFAULT '',
  `password` varchar(255) NOT NULL DEFAULT '',
  `personal_group_id` int(8) NOT NULL DEFAULT '1',
  `xcat_id` int(8) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `login_unq` (`login`),
  KEY `password_idx` (`password`),
  KEY `personal_group_id_idx` (`personal_group_id`),
  KEY `xcat_id_idx` (`xcat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_##_SECURITY_LOGINS`
--

LOCK TABLES `T_##_SECURITY_LOGINS` WRITE;
/*!40000 ALTER TABLE `T_##_SECURITY_LOGINS` DISABLE KEYS */;
INSERT INTO `T_##_SECURITY_LOGINS` VALUES (1,'nobody','',2,0),(2,'admin','{X-PBKDF2}HMACSHA2+512:AAAD6A:eS4a09+dPgPQng==:IWYciuExr1pY3XG5rYV5Ne0V2VsvyGkK/sGpBSGgLH90h3vv/Qt2lRhffu5jSrs4ObFPKGGnqe/LfX8V4mYWZg==',3,0),(3,'manager','',4,0);
/*!40000 ALTER TABLE `T_##_SECURITY_LOGINS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `T_##_SECURITY_LOGINS_TARGETS_REL`
--

DROP TABLE IF EXISTS `T_##_SECURITY_LOGINS_TARGETS_REL`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_##_SECURITY_LOGINS_TARGETS_REL` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `target_id` int(8) NOT NULL DEFAULT '0',
  `type_nick` int(4) NOT NULL DEFAULT '0',
  `login_id` int(8) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `target_id_idx` (`target_id`),
  KEY `type_nick_idx` (`type_nick`),
  KEY `login_id_idx` (`login_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_##_SECURITY_LOGINS_TARGETS_REL`
--

LOCK TABLES `T_##_SECURITY_LOGINS_TARGETS_REL` WRITE;
/*!40000 ALTER TABLE `T_##_SECURITY_LOGINS_TARGETS_REL` DISABLE KEYS */;
/*!40000 ALTER TABLE `T_##_SECURITY_LOGINS_TARGETS_REL` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `T_##_SECURITY_LOGINS_TYPES`
--

DROP TABLE IF EXISTS `T_##_SECURITY_LOGINS_TYPES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_##_SECURITY_LOGINS_TYPES` (
  `nick` varchar(20) NOT NULL DEFAULT '',
  `name` varchar(100) NOT NULL DEFAULT '',
  `target_table_name` varchar(50) NOT NULL DEFAULT '',
  `search_field_name` varchar(50) NOT NULL DEFAULT '',
  `pk_field_name` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`nick`),
  KEY `nick_idx` (`nick`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_##_SECURITY_LOGINS_TYPES`
--

LOCK TABLES `T_##_SECURITY_LOGINS_TYPES` WRITE;
/*!40000 ALTER TABLE `T_##_SECURITY_LOGINS_TYPES` DISABLE KEYS */;
/*!40000 ALTER TABLE `T_##_SECURITY_LOGINS_TYPES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `T_##_SECURITY_RIGHTS`
--

DROP TABLE IF EXISTS `T_##_SECURITY_RIGHTS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_##_SECURITY_RIGHTS` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `group_id` int(8) NOT NULL DEFAULT '0',
  `method_id` int(8) NOT NULL DEFAULT '0',
  `entity_type` int(8) NOT NULL DEFAULT '0',
  `entity_id` varchar(50) NOT NULL DEFAULT '',
  `expires` date DEFAULT '9999-01-01',
  `give_right` enum('yes','no') DEFAULT 'no',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_idx` (`group_id`,`method_id`,`entity_id`,`entity_type`),
  KEY `group_id_idx` (`group_id`),
  KEY `method_id_idx` (`method_id`),
  KEY `entity_type_idx` (`entity_type`),
  KEY `entity_id_idx` (`entity_id`),
  KEY `expires_idx` (`expires`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_##_SECURITY_RIGHTS`
--

LOCK TABLES `T_##_SECURITY_RIGHTS` WRITE;
/*!40000 ALTER TABLE `T_##_SECURITY_RIGHTS` DISABLE KEYS */;
INSERT INTO `T_##_SECURITY_RIGHTS` VALUES (1,3,1,1,'ALL','9999-01-01','yes');
/*!40000 ALTER TABLE `T_##_SECURITY_RIGHTS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `T_##_SECURITY_SEARCHES_RES`
--

DROP TABLE IF EXISTS `T_##_SECURITY_SEARCHES_RES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_##_SECURITY_SEARCHES_RES` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `group_id` int(8) NOT NULL DEFAULT '0',
  `login` varchar(20) NOT NULL DEFAULT '',
  `search_hash` int(6) NOT NULL DEFAULT '0',
  `session_hash` varchar(36) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `group_id_idx` (`group_id`),
  KEY `session_hash_idx` (`session_hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_##_SECURITY_SEARCHES_RES`
--

LOCK TABLES `T_##_SECURITY_SEARCHES_RES` WRITE;
/*!40000 ALTER TABLE `T_##_SECURITY_SEARCHES_RES` DISABLE KEYS */;
/*!40000 ALTER TABLE `T_##_SECURITY_SEARCHES_RES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `T_##_SITE_ENTRY_POINTS`
--

DROP TABLE IF EXISTS `T_##_SITE_ENTRY_POINTS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_##_SITE_ENTRY_POINTS` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `page_id` int(4) NOT NULL DEFAULT '0',
  `module_name` varchar(100) NOT NULL DEFAULT '',
  `interface_name` varchar(100) NOT NULL DEFAULT '',
  `place_name` varchar(100) NOT NULL DEFAULT '',
  `parsed` enum('yes','no') NOT NULL DEFAULT 'no',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_##_SITE_ENTRY_POINTS`
--

LOCK TABLES `T_##_SITE_ENTRY_POINTS` WRITE;
/*!40000 ALTER TABLE `T_##_SITE_ENTRY_POINTS` DISABLE KEYS */;
/*!40000 ALTER TABLE `T_##_SITE_ENTRY_POINTS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `T_##_SITE_PLUGGED_MODULES`
--

DROP TABLE IF EXISTS `T_##_SITE_PLUGGED_MODULES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_##_SITE_PLUGGED_MODULES` (
  `id` int(11) NOT NULL DEFAULT '0',
  `module_name` varchar(100) NOT NULL DEFAULT '',
  `module_descr` varchar(100) NOT NULL DEFAULT '',
  `back_office_root` varchar(100) NOT NULL DEFAULT '',
  `rights` varchar(100) DEFAULT NULL,
  `common` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_##_SITE_PLUGGED_MODULES`
--

LOCK TABLES `T_##_SITE_PLUGGED_MODULES` WRITE;
/*!40000 ALTER TABLE `T_##_SITE_PLUGGED_MODULES` DISABLE KEYS */;
INSERT INTO `T_##_SITE_PLUGGED_MODULES` VALUES (10,'XCatNew','Классы','interface/cat_classes','Classes',1),(20,'FilesNew','Хранилище файлов','interface/list','FILES',1),(30,'Secure','Система безопасности','interface/list','Secure',1),(40,'SettingsNew','Настройки сайта','admin/list','Settings',1),(90,'Seo','SEO','interface/list','SEO',0),(100,'XCatNew','Структура сайта','interface/cat_tree','Catalog',0);
/*!40000 ALTER TABLE `T_##_SITE_PLUGGED_MODULES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `T_##_SITE_TREE`
--

DROP TABLE IF EXISTS `T_##_SITE_TREE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_##_SITE_TREE` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `module_name` varchar(100) NOT NULL DEFAULT '',
  `interface_name` varchar(100) NOT NULL DEFAULT '',
  `frame_name` varchar(100) NOT NULL DEFAULT '',
  `place_name` varchar(100) NOT NULL DEFAULT '',
  `parsed` enum('yes','no') NOT NULL DEFAULT 'no',
  `prev_id` int(4) NOT NULL DEFAULT '0',
  `page_name` varchar(100) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT '',
  `descr` varchar(255) NOT NULL DEFAULT '',
  `key_words` varchar(255) NOT NULL DEFAULT '',
  `slink` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=117 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_##_SITE_TREE`
--

LOCK TABLES `T_##_SITE_TREE` WRITE;
/*!40000 ALTER TABLE `T_##_SITE_TREE` DISABLE KEYS */;
INSERT INTO `T_##_SITE_TREE` VALUES (1,'XCatNew','interface/cat_node','site_frame','center','no',0,'','Главная','','','xc_cat_node'),(2,'Main','','dev_frame','','no',0,'','Бэк оффис','','','admin'),(3,'Main','interface/header','empty_frame','center','no',0,'','','','','mn_header'),(4,'Secure','interface/list','tree','right','no',0,'','Система безопасности','','','sc_list'),(5,'Secure','interface/group_list','work','center','no',0,'','Система безопасности','','','sc_group_list'),(6,'Secure','interface/new_login','work','center','no',0,'','Новый логин','','','sc_new_login'),(7,'Secure','action/new_login','action_frame','action','no',0,'','Новый логин','','','sc_acnew_login'),(8,'Secure','interface/edit_login','work','center','no',0,'','Редактировать логин','','','sc_edit_login'),(9,'Secure','action/edit_login','action_frame','action','no',0,'','Редактировать логин','','','sc_acedit_login'),(10,'Secure','action/delete_login','action_frame','action','no',0,'','Удалить логин','','','sc_acdelete_login'),(11,'Secure','interface/new_group','work','center','no',0,'','Новая группа','','','sc_new_group'),(12,'Secure','action/new_group','action_frame','action','no',0,'','Новая группа','','','sc_acnew_group'),(13,'Secure','interface/edit_group','work','center','no',0,'','Редактировать группу','','','sc_edit_group'),(14,'Secure','action/edit_group','action_frame','action','no',0,'','Редактировать группу','','','sc_acedit_group'),(15,'Secure','interface/group_right','work','center','no',0,'','Настройка прав группы','','','sc_group_right'),(16,'Secure','interface/group_right_single','work','center','no',0,'','Настройка прав группы','','','sc_group_right_single'),(17,'Secure','action/group_right','action_frame','action','no',0,'','Настройка прав группы','','','sc_acgroup_right'),(18,'Secure','action/delete_group','action_frame','action','no',0,'','Удаление группы','','','sc_acdelete_group'),(19,'Secure','interface/login_entry','work','center','no',0,'','Настройка вхождения логина в группы','','','sc_login_entry'),(20,'Secure','action/login_entry','action_frame','action','no',0,'','Настройка вхождения логина в группы','','','sc_aclogin_entry'),(21,'Secure','interface/group_search','work','center','no',0,'','Поиск логинов/групп','','','sc_group_search'),(22,'Secure','action/group_search','action_frame','action','no',0,'','Поиск логинов/групп','','','sc_acgroup_search'),(23,'Secure','action/login','action_frame','action','no',0,'','Регистрация на сайте.','','','sc_aclogin'),(24,'SettingsNew','action/save_param','action_frame','action','no',0,'','Сохранение изменений параметра','','','st_acsave_param'),(25,'SettingsNew','action/save_params','action_frame','action','no',0,'','Сохранение изменений параметра','','','st_acsave_params'),(26,'SettingsNew','admin/list_param','work','center','no',0,'','Вывод выбранной группы параметров','','','st_adlist_param'),(27,'SettingsNew','admin/edit_param','work','center','no',0,'','Редактирование параметра','','','st_adedit_param'),(28,'SettingsNew','action/del_param','action_frame','action','no',0,'','Удаление параметра и всех его значений','','','st_addel_param'),(29,'SettingsNew','action/del_group','action_frame','action','no',0,'','Удаление грппы параметров','','','st_acdel_group'),(30,'SettingsNew','admin/add','work','center','no',0,'','Вывод формы добавления параметра','','','st_adadd'),(31,'SettingsNew','action/add','action_frame','action','no',0,'','Сохранение добавленного параметра','','','st_acadd'),(32,'SettingsNew','admin/list','tree','right','no',0,'','Список настроек','','','st_adlist'),(33,'Main','interface/header','empty_frame','center','no',0,'','','','','mn_header'),(34,'XCatNew','interface/cat_classes','tree','right','no',0,'','','','','xc_cat_classes'),(35,'XCatNew','interface/cat_tree','tree','right','no',0,'','','','','xc_cat_tree'),(36,'XCatNew','interface/attrs','work','center','no',0,'','','','','xc_attrs'),(37,'XCatNew','interface/new_class','work','center','no',0,'','','','','xc_new_class'),(38,'XCatNew','action/copy_class','action_frame','action','no',0,'','','','','xc_accopy_class'),(39,'XCatNew','interface/copy_class','work','center','no',0,'','','','','xc_copy_class'),(40,'XCatNew','action/new_class','action_frame','action','no',0,'','','','','xc_acnew_class'),(41,'XCatNew','interface/new_attr','work','center','no',0,'','','','','xc_new_attr'),(42,'XCatNew','action/new_attr','action_frame','action','no',0,'','','','','xc_acnew_attr'),(43,'XCatNew','interface/ed_attr','work','center','no',0,'','','','','xc_ed_attr'),(44,'XCatNew','action/ed_attr','action_frame','action','no',0,'','','','','xc_aced_attr'),(45,'XCatNew','interface/ed_class','work','center','no',0,'','','','','xc_ed_class'),(46,'XCatNew','interface/ed_class_chk','work','center','no',0,'','','','','xc_ed_class_chk'),(47,'XCatNew','action/ed_class','action_frame','action','no',0,'','','','','xc_aced_class'),(48,'XCatNew','action/new_attr_check','action_frame','action','no',0,'','','','','xc_acnew_attr_check'),(49,'XCatNew','action/attr_down_order','action_frame','action','no',0,'','','','','xc_acattr_down_order'),(50,'XCatNew','action/attr_up_order','action_frame','action','no',0,'','','','','xc_acattr_up_order'),(51,'XCatNew','action/attrs_ins_group','action_frame','action','no',0,'','','','','xc_acattrs_ins_group'),(52,'XCatNew','interface/ed_attr_chk','work','center','no',0,'','','','','xc_ed_attr_chk'),(53,'XCatNew','interface/archive','work','center','no',0,'','','','','xc_archive'),(54,'XCatNew','interface/copy_class_from_arch','work','center','no',0,'','','','','xc_copy_class_from_arch'),(55,'XCatNew','action/copy_class_from_arch','action_frame','action','no',0,'','','','','xc_accopy_class_from_arch'),(56,'XCatNew','action/del_class','action_frame','action','no',0,'','','','','xc_acdel_class'),(57,'XCatNew','interface/ed_group','work','center','no',0,'','','','','xc_ed_group'),(58,'XCatNew','interface/ed_attr_grp','work','center','no',0,'','','','','xc_ed_attr_grp'),(59,'XCatNew','action/del_attr','action_frame','action','no',0,'','','','','xc_acdel_attr'),(60,'XCatNew','action/ed_class','action_frame','action','no',0,'','','','','xc_aced_class'),(61,'XCatNew','interface/show_object','work','center','no',0,'','','','','xc_show_object'),(62,'XCatNew','interface/view_object','work','center','no',0,'','','','','xc_view_object'),(63,'XCatNew','interface/new_object','work','center','no',0,'','','','','xc_new_object'),(64,'XCatNew','action/new_object','action_frame','action','no',0,'','','','','xc_acnew_object'),(65,'XCatNew','action/del_object','action_frame','action','no',0,'','','','','xc_acdel_object'),(66,'XCatNew','interface/ed_object','work','center','no',0,'','','','','xc_ed_object'),(67,'XCatNew','action/ed_object','action_frame','action','no',0,'','','','','xc_aced_object'),(68,'XCatNew','action/ch_object_publ','action_frame','action','no',0,'','','','','xc_acch_object_publ'),(69,'XCatNew','action/ch_object_ord','action_frame','action','no',0,'','','','','xc_acch_object_ord'),(70,'XCatNew','interface/copy_class_to_arch','work','center','no',0,'','','','','xc_copy_class_to_arch'),(71,'XCatNew','action/copy_class_to_arch','action_frame','action','no',0,'','','','','xc_accopy_class_to_arch'),(72,'XCatNew','action/del_class_from_arch','action_frame','action','no',0,'','','','','xc_acdel_class_from_arch'),(73,'XCatNew','action/ed_attr_chk','action_frame','action','no',0,'','','','','xc_aced_attr_chk'),(74,'XCatNew','action/new_attr_group','action_frame','action','no',0,'','','','','xc_acnew_attr_group'),(75,'XCatNew','action/del_attr_group','action_frame','action','no',0,'','','','','xc_acdel_attr_group'),(76,'XCatNew','action/ch_grp_4_grp','action_frame','action','no',0,'','','','','xc_acch_grp_4_grp'),(77,'XCatNew','action/ed_attr_group','action_frame','action','no',0,'','','','','xc_aced_attr_group'),(78,'XCatNew','action/ch_group','action_frame','action','no',0,'','','','','xc_acch_group'),(79,'XCatNew','interface/copy_tree','tree','right','no',0,'','','','','xc_copy_tree'),(80,'XCatNew','action/transit_object','action_frame','action','no',0,'','','','','xc_actransit_object'),(81,'XCatNew','interface/show_found','work','center','no',0,'','','','','xc_show_found'),(82,'XCatNew','404','site_frame','center','no',0,'','','','','xc_404'),(83,'Main','action/login','action_frame','action','no',0,'','','','','mn_aclogin'),(84,'Main','action/logout','action_frame','action','no',0,'','','','','mn_aclogout'),(85,'XCatNew','interface/file_select','empty_frame','center','no',0,'','','','','xc_file_select'),(86,'XCatNew','interface/view_object','work','center','no',0,'','','','','xc_view_object'),(87,'FilesNew','interface/cat_nav','work','center','no',0,'','Управление картиками и файлами','','','fl_cat_nav'),(88,'FilesNew','action/trash_clean','action_frame','action','no',0,'','Очистка корзины','','','fl_actrash_clean'),(89,'FilesNew','action/file_del','action_frame','action','no',0,'','Удаление файла','','','fl_acfile_del'),(90,'FilesNew','action/file_edit_save','action_frame','action','no',0,'','Сохранение отредактированного файла','','','fl_acfile_edit_save'),(91,'FilesNew','interface/file_edit','work','center','no',0,'','Редактирование названия и описания файлов','','','fl_file_edit'),(92,'FilesNew','interface/file_add','work','center','no',0,'','Добавление файла','','','fl_file_add'),(93,'FilesNew','action/cat_del','action_frame','action','no',0,'','Удаление раздела','','','fl_accat_del'),(94,'FilesNew','action/cat_edit_save','action_frame','action','no',0,'','Сохранение отредактированного раздела','','','fl_accat_edit_save'),(95,'FilesNew','interface/cat_edit','work','center','no',0,'','Редактирование раздела','','','fl_cat_edit'),(96,'FilesNew','action/cat_add_save','action_frame','action','no',0,'','Сохранение добавляемого раздела','','','fl_accat_add_sace'),(97,'FilesNew','interface/cat_add','work','center','no',0,'','Добавление раздела','','','fl_cat_add'),(98,'FilesNew','interface/list','tree','right','no',0,'','Вывод содержимого раздела','','','fl_list'),(99,'FilesNew','action/new_file','action_frame','action','no',0,'','Новый файл','','','fl_acnew_file'),(100,'FilesNew','interface/cat_trans','empty_frame','center','no',0,'','','','','fl_cat_trans'),(101,'FilesNew','interface/cat_tree','tree','right','no',0,'','','','','fl_cat_tree'),(102,'FilesNew','interface/file_local','empty_frame','center','no',0,'','','','','fl_file_local'),(103,'FilesNew','interface/file_select','empty_frame','center','no',0,'','','','','fl_file_select'),(104,'Seo','interface/list','tree','right','no',0,'','','','','seo_list'),(105,'Seo','interface/structure','work','center','no',0,'','Просмотр структуры','','','seo_structure'),(106,'Seo','interface/metatags','work','center','no',0,'','Метатеги','','','seo_metatags'),(107,'Seo','interface/sitemaps','work','center','no',0,'','Карта сайта','','','seo_sitemaps'),(108,'Seo','interface/robots','work','center','no',0,'','Файл robots.txt','','','seo_robots'),(109,'Seo','interface/broken','work','center','no',0,'','Поиск битых ссылок','','','seo_broken'),(110,'XCatNew','interface/import_class','work','center','no',0,'','Импорт класса','','','xc_import_class'),(111,'XCatNew','action/import_class','action_frame','action','no',0,'','','','','xc_acimport_class'),(112,'XCatNew','action/export_class','action_frame','action','no',0,'','','','','xc_acexport_class'),(113,'XCatNew','interface/object_gallery','work','center','no',0,'','','','','xc_object_gallery'),(114,'XCatNew','interface/object_gallery_photo_select','empty_frame','center','no',0,'','','','','xc_obj_gal_photos_sel'),(115,'XCatNew','interface/gallery_pic_select','empty_frame','center','no',0,'','','','','xc_obj_gal_photo_sel'),(116,'Seo','interface/redirects','work','center','no',0,'','Редиректы','','','seo_redirects');
/*!40000 ALTER TABLE `T_##_SITE_TREE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `T_##_SYSTEM_PARAMS`
--

DROP TABLE IF EXISTS `T_##_SYSTEM_PARAMS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_##_SYSTEM_PARAMS` (
  `param_name` varchar(254) NOT NULL DEFAULT '',
  `description_name` varchar(254) NOT NULL DEFAULT '',
  PRIMARY KEY (`param_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_##_SYSTEM_PARAMS`
--

LOCK TABLES `T_##_SYSTEM_PARAMS` WRITE;
/*!40000 ALTER TABLE `T_##_SYSTEM_PARAMS` DISABLE KEYS */;
INSERT INTO `T_##_SYSTEM_PARAMS` VALUES ('Cookies.LastKill','Last cookies garbage collector time'),('Files.FilesPerPage','Колличество картинок на странице'),('Forms.AgreementComment','Комментарий к согласию'),('Forms.AgreementLabel','Подпись для согласия с условиями'),('Forms.CaptchaComment','Комментарий к каптче'),('Forms.CaptchaLabel','Подпись для каптчи'),('Forms.EmailFrom','Адрес с которого приходят письма'),('Forms.RemarkText','Дополнительная приписка в форме '),('Forms.SubmitLabel','Подпись для кнопки отправки формы '),('Seo.Description','Устанавливает общее описание для всех страниц сайта.'),('Seo.Domain','Имя домена. Должно быть заполнено.'),('Seo.Keywords','Устанавливает общие ключевые слова для всех страниц сайта.'),('Seo.Scheme','Схема http для построения карты сайта (http/https).'),('Seo.SiteName','Устанавливает общее для всех страниц название сайта.'),('Seo.Title','Устанавливает общий заголовок для всех страниц сайта.'),('Seo.UseRedirects','Использовать редиректы? (YES/NO)'),('Session.DeleteExtention','Расширеное удаление \"мертвых сессий\"'),('Session.EmptySessionIDValue','Значение \"пустого\" сессионого УИДа'),('Session.LiveTime','Время жизни сессии'),('Session.NobodyUID','УИД пользователя по умолчанию'),('Session.SessionIDRegexpConvertion','Конверсия сессоного УИДа с помощью regexp-ов'),('Session.UseEmptySessionID','Использовать ли пустой сессион хеш по умолчанию'),('XCatalog.AttrsPerPage','Атрибутов класса на страницу'),('XCatalog.ObjectsPerPage','Объектов на страницу');
/*!40000 ALTER TABLE `T_##_SYSTEM_PARAMS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `T_##_SYS_ERROR`
--

DROP TABLE IF EXISTS `T_##_SYS_ERROR`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_##_SYS_ERROR` (
  `error` varchar(128) NOT NULL DEFAULT '',
  `message` varchar(100) NOT NULL DEFAULT '',
  `description` varchar(254) NOT NULL DEFAULT '0',
  PRIMARY KEY (`error`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_##_SYS_ERROR`
--

LOCK TABLES `T_##_SYS_ERROR` WRITE;
/*!40000 ALTER TABLE `T_##_SYS_ERROR` DISABLE KEYS */;
/*!40000 ALTER TABLE `T_##_SYS_ERROR` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `T_##_SYS_PARAMS_VAL`
--

DROP TABLE IF EXISTS `T_##_SYS_PARAMS_VAL`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_##_SYS_PARAMS_VAL` (
  `value_index` varchar(128) NOT NULL DEFAULT '',
  `param_name` varchar(254) NOT NULL DEFAULT '',
  `value` varchar(254) DEFAULT '',
  PRIMARY KEY (`param_name`,`value_index`),
  KEY `param_name` (`param_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_##_SYS_PARAMS_VAL`
--

LOCK TABLES `T_##_SYS_PARAMS_VAL` WRITE;
/*!40000 ALTER TABLE `T_##_SYS_PARAMS_VAL` DISABLE KEYS */;
INSERT INTO `T_##_SYS_PARAMS_VAL` VALUES ('','Cookies.LastKill',''),('','Files.FilesPerPage','30'),('','Forms.AgreementComment',''),('','Forms.AgreementLabel','Я согласен с политикой компании в отношении обработки персональных данных'),('','Forms.CaptchaComment',''),('','Forms.CaptchaLabel','Введите число цифрами'),('','Forms.EmailFrom',''),('','Forms.RemarkText','* отмечены поля, обязательные для заполнения'),('','Forms.SubmitLabel','Отправить'),('','Seo.Description',''),('','Seo.Domain',''),('','Seo.Keywords',''),('','Seo.Scheme',''),('','Seo.SiteName',''),('','Seo.Title',''),('','Seo.UseRedirects','NO'),('','Session.DeleteExtention','SECURE_DELETE_SEARCHES_RES'),('','Session.EmptySessionIDValue',''),('','Session.LiveTime','7200'),('','Session.NobodyUID','1'),('','Session.SessionIDRegexpConvertion','7200'),('','Session.UseEmptySessionID','YES'),('','XCatalog.AttrsPerPage','40'),('','XCatalog.ObjectsPerPage','20');
/*!40000 ALTER TABLE `T_##_SYS_PARAMS_VAL` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `T_##_USER_SETTINGS`
--

DROP TABLE IF EXISTS `T_##_USER_SETTINGS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_##_USER_SETTINGS` (
  `uid` int(8) NOT NULL DEFAULT '0',
  `vars` blob,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_##_USER_SETTINGS`
--

LOCK TABLES `T_##_USER_SETTINGS` WRITE;
/*!40000 ALTER TABLE `T_##_USER_SETTINGS` DISABLE KEYS */;
/*!40000 ALTER TABLE `T_##_USER_SETTINGS` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-10-10 11:39:16
