package _projectName_::controllers::__page_404;

use utf8;
use strict;

use base qw(_projectName_::controllers::__site__);


sub new {
    my $proto = shift;
    my $class = ref($proto) || $proto;

    my $self = $class->SUPER::new(@_);
    bless $self, $class;

    return $self;
}

1;
