package _projectName_::controllers::home_page;

use utf8;
use strict;

use base qw(_projectName_::controllers::__site__);

use Engine::GlobalVars;


sub new {
    my $proto = shift;
    my $class = ref($proto) || $proto;

    my $self = $class->SUPER::new(@_);
    bless $self, $class;

    return $self;
}

sub Init {
    my $self = shift;
    $self->SUPER::Init(@_);

    if (my $obj = $self->GetObject($App->Param('node_id'))) {
        $self->AddData({ object => $obj });
    }
}

sub GetObject {
    my $self = shift;
    my ($object_id) = @_;

    if (my $obj = $DB->CAT_GET_OBJECT_WITH_ATTRS(object_id => $object_id, attrs => ['content'])->fetchrow_hashref()) {
        return $obj;
    }
    return undef;
}

1;
