package _projectName_::controllers::page_with_form;

use utf8;
use strict;

use base qw(_projectName_::controllers::__site__);

use Engine::GlobalVars;


sub new {
    my $proto = shift;
    my $class = ref($proto) || $proto;

    my $self = $class->SUPER::new(@_);
    bless $self, $class;

    return $self;
}

sub Init {
    my $self = shift;
    $self->SUPER::Init(@_);

    if (my $obj = $self->GetObject($App->Param('node_id'))) {
        $self->AddData({ object => $obj });
    }
}

sub GetObject {
    my $self = shift;
    my ($object_id, $params) = @_;

    if (my $obj = $DB->CAT_GET_OBJECT_WITH_ATTRS(object_id => $object_id, attrs => ['goal', '_content', '_s_content', '_form_class_name'])->fetchrow_hashref()) {
        return $self->ProcessForm($obj, $params);
    }
    return undef;
}

1;
